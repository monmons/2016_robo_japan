/* 
 * File:   PIC24FJxxxGC006_head.h
 * Author: Shuuya
 *
 * Created on 2016/08/09, 21:20
 */

#ifndef PIC24FJXXXGC006_HEAD_H
#define	PIC24FJXXXGC006_HEAD_H

#ifdef	__cplusplus
extern "C" {
#endif


#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "defines_for_PIC24F.h"
#include "LimitSwitch.h"
#include "mcc_generated_files/pin_manager.h"
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>

    void ADC(void); //関数定義
    void Nx10_ms  (uint8_t);
    void Nx100_ms (uint8_t); //関数定義

    /*UART*/

#ifndef UART1_bps
#define UART1_bps (115200.0)//UART1のボーレート
#endif    

#ifndef UART2_bps
#define UART2_bps (115200.0)//UART2のボーレート
#endif  

#ifndef UART3_bps
#define UART3_bps (115200.0)//UART3のボーレート
#endif

#ifndef UART4_bps
#define UART4_bps (115200.0)//UART4のボーレート
#endif
    /* eight_bitsUARTtype:8bit通信  nine_bitsUARTtype:9bit通信 */
#ifndef UART1_type
#define UART1_type eight_bitsUARTtype//8bitもしくは9bitの通信
#endif  

#ifndef UART2_type
#define UART2_type eight_bitsUARTtype//8bitもしくは9bitの通信
#endif  

#ifndef UART3_type
#define UART3_type eight_bitsUARTtype//8bitもしくは9bitの通信
#endif 

#ifndef UART4_type
#define UART4_type eight_bitsUARTtype//8bitもしくは9bitの通信
#endif 


    /* I2C */
#define Fscl (400000.0)//I2C周波数
#define I2C_BAUD ((FCY / (2.0 * Fscl))- 2.0)

    /*OutputCompare*/

#ifndef OC_1and2_frequency
#define OC_1and2_frequency (10000.0)//OC1,2のPWM周波数(単位:Hz,小数点をつける),TMR2をタイマーベースとしている
#endif  

#ifndef OC_1and2_period
#define OC_1and2_period (( (1.0) / (OC_1and2_frequency)) )//OC1,2のPWM周波数(単位:us,小数点をつける),TMR2をタイマーベースとしている
#endif  

#ifndef OC_3and4_frequency
#define OC_3and4_frequency (10000.0)//OC3,4のPWM周波数(単位:Hz,小数点をつける),TMR3をタイマーベースとしている
#endif      

#ifndef OC_3and4_period
#define OC_3and4_period 0 //((1/(OC_3and4_frequency))*1000000)//OC3,4のPWM周期(単位:us,小数点をつける),TMR3をタイマーベースとしている
#endif  

    /*Moter_OutputCompare (1,2と3,4のOCは同じタイマーピリオド)*/

#ifndef OC_for_MOTER_A
#define OC_for_MOTER_A 1//MOTER_Aに使うOCの番号
#endif 

#ifndef OC_for_MOTER_B
#define OC_for_MOTER_B 2//MOTER_Bに使うOCの番号
#endif 

#ifndef LAT_for_MOTER_A1
#define LAT_for_MOTER_A1 (LATEbits.LATE5)//モータの正転にて使うポート
#endif  

#ifndef LAT_for_MOTER_A2
#define LAT_for_MOTER_A2 (LATEbits.LATE6)//モータの逆転にて使うポート
#endif 

#ifndef LAT_for_MOTER_B1
#define LAT_for_MOTER_B1 (LATEbits.LATE7)//モータの正転にて使うポート
#endif 

#ifndef LAT_for_MOTER_B2
#define LAT_for_MOTER_B2 (LATGbits.LATG6)//モータの逆転にて使うポート
#endif 

#ifndef LAT_for_MOTER_C1
#define LAT_for_MOTER_C1 (LATBbits.LATB0)//モータの正転にて使うポート
#endif  

#ifndef LAT_for_MOTER_C2
#define LAT_for_MOTER_C2 (LATBbits.LATB1)//モータの逆転にて使うポート
#endif 

#ifndef LAT_for_MOTER_D1
#define LAT_for_MOTER_D1 (LATBbits.LATB2)//モータの正転にて使うポート
#endif 

#ifndef LAT_for_MOTER_D2
#define LAT_for_MOTER_D2 (LATBbits.LATB3)//モータの逆転にて使うポート
#endif 
    /*各モジュールの割り込み優先度(割り込みを使用しない場合 0　と書いてください , 0~7) モジュール自体を使用しない場合 not_used_moduleとしてください*/

    /*UART*/
#ifndef UART1TX_IP
#define UART1TX_IP 3
#endif 

#ifndef UART1RX_IP
#define UART1RX_IP 3
#endif 

#ifndef UART2TX_IP
#define UART2TX_IP 5
#endif     

#ifndef UART2RX_IP
#define UART2RX_IP 4
#endif     

#ifndef UART3TX_IP
#define UART3TX_IP 0
#endif 

#ifndef UART3RX_IP
#define UART3RX_IP 5
#endif 

#ifndef UART4TX_IP
#define UART4TX_IP 0
#endif     

#ifndef UART4RX_IP
#define UART4RX_IP 2
#endif     

    /*TMR*/
#ifndef TMR1_IP
#define TMR1_IP 1
#endif     

#ifndef TMR2_IP
#define TMR2_IP 0
#endif     

#ifndef TMR3_IP
#define TMR3_IP 2
#endif     

#ifndef TMR4_IP
#define TMR4_IP 1
#endif     

#ifndef TMR5_IP
#define TMR5_IP 4
#endif 

    /*I2C(EEPROM用)*/
#ifndef I2CM_IP
#define I2CM_IP 3
#endif      

    /*以下は変更しなくてよい*/

    /*マクロ関数*/
#define setupI2C_M(){\
    _TRISD9 = 1;\
    _TRISD10 = 1;\
    PMD1bits.I2C1MD=0;\
    I2C1CON=0;\
    I2C1ADD=0;\
    I2C1MSK=0;\
    I2C1BRG=I2C_BAUD;\
    I2C1CONbits.I2CEN=1;\
    _MI2C1IF=0;\
    _MI2C1IP=I2CM_IP;\
    _MI2C1IE=1;\
    __delay_ms(1);}


#define putI2C_M (datas) {IFS1bits.MI2C1IF=0; I2C1TRN = datas;}    

#define putUART1(datas) {_U1TXIF = 0; U1TXREG = datas;}
#define putUART2(datas) {_U2TXIF = 0; U2TXREG = datas;}
#define putUART3(datas) {_U3TXIF = 0; U3TXREG = datas;}
#define putUART4(datas) {_U4TXIF = 0; U4TXREG = datas;}
#define putUART1_Bluetooth(datas) {Bluetooth1_TXIF = 0; Bluetooth1_TXREG = datas;}
#define putUART2_Bluetooth(datas) {Bluetooth2_TXIF = 0; Bluetooth2_TXREG = datas;}


#define SET_TRUN_NORMAL_A()     {MOTOR_LAT_A1_SetHigh();MOTOR_LAT_A2_SetLow();}
#define SET_TRUN_REVERSE_A()    {MOTOR_LAT_A1_SetLow();MOTOR_LAT_A2_SetHigh();}
#define SET_BRAKE_A()           {MOTOR_LAT_A1_SetLow();MOTOR_LAT_A2_SetLow();}

#define SET_TRUN_NORMAL_B()     {MOTOR_LAT_B1_SetHigh();MOTOR_LAT_B2_SetLow();}
#define SET_TRUN_REVERSE_B()    {MOTOR_LAT_B1_SetLow();MOTOR_LAT_B2_SetHigh();}
#define SET_BRAKE_B()           {MOTOR_LAT_B1_SetLow();MOTOR_LAT_B2_SetLow();}   

#define SET_TRUN_NORMAL_C()     {MOTOR_LAT_C1_SetHigh();MOTOR_LAT_C2_SetLow();}
#define SET_TRUN_REVERSE_C()    {MOTOR_LAT_C1_SetLow();MOTOR_LAT_C2_SetHigh();}
#define SET_BRAKE_C()           {MOTOR_LAT_C1_SetLow();MOTOR_LAT_C2_SetLow();}   

#define SET_TRUN_NORMAL_D()     {MOTOR_LAT_D1_SetHigh();MOTOR_LAT_D2_SetLow();} 
#define SET_TRUN_REVERSE_D()    {MOTOR_LAT_D1_SetLow();MOTOR_LAT_D2_SetHigh();} 
#define SET_BRAKE_D()           {MOTOR_LAT_D1_SetLow();MOTOR_LAT_D2_SetLow();}  

#define init_MOTOR(){\
    setMotor_State(1,0.0);\
    setMotor_State(2,0.0);\
    setMotor_State(3,0.0);\
    setMotor_State(4,0.0);\
}

#define time_out_controller(){\
    setupBluetooth1_slave();\
    init_controller_data();\
    _T3IE=0;\
}

    typedef union {
        uint8_t byte;

        struct {
            unsigned useful : 1; //受信データ群が有効かどうか
            unsigned recievetime : 7; //受信回数
        };

    } Controler_status;

    typedef struct PID_var {
        int16_t* DrpmA;
        int16_t* DrpmB;

        float* P_A_gein;
        float* I_A_gein;
        float* D_A_gein;

        float* P_B_gein;
        float* I_B_gein;
        float* D_B_gein;

        int8_t* DrpmA_up_speed;
        int8_t* DrpmB_up_speed;

        int16_t* now_DrpmA;
        int16_t* now_DrpmB;

        float* now_rpmA1;
        float* now_rpmB1;

        float* now_dutyA;
        float* now_dutyB;

    } PID_variables;

    extern uint16_t T2CKPS;
    extern uint16_t T3CKPS;
    extern uint8_t Controller_variables[];
    extern float result[];

    void setup_ports();
    void setupUART(void);
    void I2Cwrite_M();
    //void setupOC(void); //PWM周期(単位:ms)を引数とする,TMR2をタイマーベースとしている
    void setdutySec_for_OC(unsigned char, bool, unsigned short); //(動かすOCの番号,アクティブハイかどうか,パルス幅(μs))
    void setduty_OC(uint8_t, bool, float); //(動かすOCの番号,アクティブハイかどうか,duty_percent):::dutyの絶対値をPR2より小さくする
    void setMotor_State(uint8_t, float);
    void setupTMR(void);
    void setupTMR3(void);
    void setdelay(unsigned char, unsigned char, float, bool); //単位:μs,(モード,作りたい時間)モード1:milliSec,モード2:microSec,モード3:nanoSec  **注意**短すぎたり,長すぎるとオーバーフローします

    void setupQEI(void);

    bool check_data_Controler(uint8_t*, uint8_t);
    bool Controler_checker_ISR(uint8_t); //確認の合否を返り値とする(チェック動作の変化,モード２で必要なポインタ)    モード１:割り込みなし時の'S'２回を待つ動作,モード２:コントローラからのデータを確認する
    void init_controller_data(void);

    unsigned char makeASCII(unsigned char); //0~9までの10進数をASCIIコードに変える
    void makeASCIIs(uint8_t, int16_t, uint8_t*); //signed short型の変数を,分割,ASCIIコードへの変換を行い,char型のポインタに入れる,(１番目の変数が進数の値,変換したい変数,変換後のデータを入れるポインタ型(要素数：7))
    void tmr_ISR_PID_Control(PID_variables*);

    float* mecanum_Control(bool, uint8_t, uint8_t); //Go or trun,x,y


#ifdef	__cplusplus
}
#endif

#endif	/* PIC24FJXXXGC006_HEAD_H */

