#ifndef FCY
#define FCY 16000000
#endif

#include "mcc_generated_files/mcc.h"
#include "function_2016_japancap.h"
#include "mcc_generated_files/pin_manager.h"
#include "constConfig.h"
#include <stdio.h>

extern float flontLegDelay;
extern float kondoOffset[];
extern float kondoPositions[];
extern float wheelsLimits[];
extern float liftDutiesDown;
extern float liftDutiesUp;
extern float liftDutiesDownOrigin;
extern float liftDutiesUpOrigin;
extern float kickMecanumF;
extern float kickMecanumB;
extern float kickCenter;
extern float kickTime;
extern float liftPositions[];
extern float liftMax;
extern float liftMin;
//これは、今回のマシンの各部位の動きを関数化するファイルです
bool safety_lock = false;
bool useful_controller = false;

static wheelState wheelMode = wheel_gradual;
static wheelSpeed speedState = wheel_normal;
const char* const wheelMes[] = {
    "Grad",
    "Lock",
    "Prom"
};
static float mecanum_Dduty[4] = {};
static float To_change_necessary_time_all = 250.0; //単位はms,???msで目標値へ
//static float turn_speed_percent = 50.0; //旋回時のpercent

void init_mecanum_datas(void) {



}//各種データのEEPROMからの読み込み

void mecanum_control_2016(void) {

    float x_calculation = 0.0, y_calculation = 0.0; //符号付きの処理

    x_calculation = ((int8_t) (horizontal_lever_right - 128));
    y_calculation = ((int8_t) (perpendicular_lever_right - 128)); //なんで-?

    float duty_FR = 0.0, duty_FL = 0.0;
    float duty_BR = 0.0, duty_BL = 0.0;
    //F:flont(前) B:back(後)
    //R:right(右) L:left(左)
    //例:FR(前右)
    //uint8_t i = 0;

#if 0
    /* 円形不感領域 */
    if ((pow(x_calculation, 2.0) + pow(y_calculation, 2.0)) <= pow(min_go_area, 2.0)) {
        x_calculation = 0.0;
        y_calculation = 0.0;
    }// (x^2) + (y^2) <= r^2 のとき 反応しない 
#endif
    /* 正方形不感領域*/
    if ((fabs(x_calculation)) <= (fabs(min_go_area))) {
        x_calculation = 0.0;
    }
    if ((fabs(y_calculation)) <= (fabs(min_go_area))) {
        y_calculation = 0.0;
    }

    /******************     上限設定１        ******************************/
    /*************/
    /* x値の調整 */
    /*************/
    if (x_calculation > 0.0) {
        if (x_calculation > max_go_area) {
            x_calculation = max_go_area - min_go_area;
        } else {
            x_calculation = (x_calculation - min_go_area); //不感領域分を引く
        }
    }//正のとき
    else if (x_calculation < 0.0) {
        if (x_calculation < -max_go_area) {
            x_calculation = -max_go_area + min_go_area;
        } else {
            x_calculation = (x_calculation + min_go_area); //不感領域分を引く
        }
    }//負のとき
    /************/
    /* y値の調整 */
    /************/
    if (y_calculation > 0.0) {
        if (y_calculation > max_go_area) {
            y_calculation = max_go_area - min_go_area;
        } else {
            y_calculation = (y_calculation - min_go_area); //不感領域分を引く
        }
    }//正のとき
    else if (y_calculation < 0.0) {
        if (y_calculation < -max_go_area) {
            y_calculation = -max_go_area + min_go_area;
        } else {
            y_calculation = (y_calculation + min_go_area); //不感領域分を引く
        }
    }//負のとき
    /**************************************************************************/
    /******************     途中計算 終了   ***********************************/
    /**************************************************************************/
    /* 斜線領域区分 (直線運動用) */
    if ((fabs(y_calculation)) >= (fabs(x_calculation))) {//区分領域:上下
        duty_FR = y_calculation;
        duty_FL = y_calculation;
    } else {//区分領域:左右
        duty_FR = ((-1.0) * x_calculation);
        duty_FL = x_calculation;
    }
    duty_BR = duty_FL; //BRはFLと同じ動き
    duty_BL = duty_FR; //BLはFRと同じ動き
    /**************************************************************************/
    /* 旋回 */
    float x_truning_num = ((float) ((int8_t) (horizontal_lever_left - 128)));

    /* 不感帯 */
    if ((fabs(x_truning_num)) <= (fabs(min_go_area))) {
        x_truning_num = 0.0;
    }

    /*************/
    /* x値の調整 */
    /*************/
    if (x_truning_num > 0.0) {
        if (x_truning_num > max_go_area) {
            x_truning_num = max_go_area - min_go_area;
        } else {
            x_truning_num = (x_truning_num - min_go_area); //不感領域分を引く
        }
    }//正のとき
    else if (x_truning_num < 0.0) {
        if (x_truning_num < -max_go_area) {
            x_truning_num = -max_go_area + min_go_area;
        } else {
            x_truning_num = (x_truning_num + min_go_area); //不感領域分を引く
        }
    }//負のとき

    duty_FR -= x_truning_num;
    duty_FL += x_truning_num;
    duty_BR -= x_truning_num;
    duty_BL += x_truning_num;

    /**************************************************************************/
    //加速、減速
#if 1
    if (slowButton) {
        speedState = wheel_slow;
    } else if (accelButton) {
        speedState = wheel_fast;
    } else {
        speedState = wheel_normal;
    }
#endif
    duty_FR /= (max_go_area - min_go_area);
    duty_FL /= (max_go_area - min_go_area);
    duty_BR /= (max_go_area - min_go_area);
    duty_BL /= (max_go_area - min_go_area);
    /**************************************************************************/
    /*      duty比の上限の設定      */
    if (duty_FR > 1.0) {
        duty_FR = 1.0;
    } else if (duty_FR < -1.0) {
        duty_FR = -1.0;
    }
    if (duty_FL > 1.0) {
        duty_FL = 1.0;
    } else if (duty_FL < -1.0) {
        duty_FL = -1.0;
    }
    if (duty_BR > 1.0) {
        duty_BR = 1.0;
    } else if (duty_BR < -1.0) {
        duty_BR = -1.0;
    }
    if (duty_BL > 1.0) {
        duty_BL = 1.0;
    } else if (duty_BL < -1.0) {
        duty_BL = -1.0;
    }
    /**************************************************************************/

    /* 単位:% 1.0 ~ -1.0 */

    mecanum_Dduty[0] = duty_FR * wheelsLimits[speedState];
    mecanum_Dduty[1] = duty_FL * wheelsLimits[speedState];
    mecanum_Dduty[2] = duty_BR * wheelsLimits[speedState];
    mecanum_Dduty[3] = duty_BL * wheelsLimits[speedState];

#if 0
    for (i = 0; i < 4; i++) {
        setMotor_State((i + 1), (mecanum_Dduty[i] / 1.0));
        //setMotor_State((i + 1), (0.8));
    }
#endif

}

static float mecanum_Pduty[4] = {}; //強制モードのデューティ
static float center_Pduty[2] = {}; //強制モードのデューティ

void setPromptDuty(float fr, float fl, float br, float bl, float mr, float ml) {
    mecanum_Pduty[0] = fr;
    mecanum_Pduty[1] = fl;
    mecanum_Pduty[2] = br;
    mecanum_Pduty[3] = bl;
    center_Pduty[0] = mr;
    center_Pduty[1] = ml;
}

void center_TMR_ISR() {

    static float now_duty[2] = {}; //現在のduty
    static float up_duty[2] = {}; //現在の必要上昇率
    static float now_deviation[2] = {}; //偏差
    static float center_Dduty[2] = {}; //目標値
    float To_change_necessary_time = 0.0; //目標値までの必要時間(単位時間:10ms)
    uint8_t Max_deviation = 0; //最大偏差の番号
    uint8_t i = 0, t = 0; //for文用
    switch (wheelMode) {
        case wheel_gradual:
        case wheel_gradual_lock:
            /**************************************************************************/
            /* <偏差の計算 & 最大偏差の検索> */
            /* <偏差の計算> */
            /**************************************************************************/
            for (t = 0; t < 2; t++) {
                if ((wheelMode == wheel_gradual)&&(mecanum_Dduty[0] == mecanum_Dduty[2]) && (mecanum_Dduty[1] == mecanum_Dduty[3])&&(!safety_lock)) {
                    center_Dduty[t] = mecanum_Dduty[t];
                } else {
                    center_Dduty[t] = 0.0;
                }
                now_deviation[t] = center_Dduty[t] - now_duty[t];

            }
            /* <最大偏差の検索> */
            for (t = 0; t < 2; t++) {
                if ((fabs(now_deviation[t])) > (fabs(now_deviation[Max_deviation]))) {
                    Max_deviation = t;
                }
            }
            /**************************************************************************/

            /* <必要時間の計算> */
            if (now_deviation[Max_deviation] != 0.0) {
                /* <必要時間の計算> */
                To_change_necessary_time = ((To_change_necessary_time_all) * (fabs(now_deviation[Max_deviation]))); //[ms]
                /* <上昇率の計算> */
                for (i = 0; i < 2; i++) {
                    up_duty[i] = ((now_deviation[i]) / (To_change_necessary_time)) * (10.0); //[duty/ms * 10.0]
                }
            } else {
                for (i = 0; i < 2; i++) {
                    up_duty[i] = 0.0; //上昇の停止
                }
            }
            /******************************************************************************/
            /* <dutyの確定 & 次回呼び出しへの準備> */
            for (i = 0; i < 2; i++) {
                /* <dutyの上昇> */
                now_duty[i] = now_duty[i] + up_duty[i];

                /* <上限、下限の設定> */
                if (((up_duty[i] > 0.0) && (now_duty[i] > center_Dduty[i])) ||
                        ((up_duty[i] < 0.0) && (now_duty[i] < center_Dduty[i]))) {
                    up_duty[i] = 0.0;
                    now_duty[i] = center_Dduty[i];
                }
            }
            break;
        case wheel_prompt:
            for (i = 0; i < 2; i++) {
                now_duty[i] = center_Pduty[i];
            }
            break;
    }
    /* <中輪へのdutyを確定> */
    dutyA_percent_RS422M(0) = now_duty[0];
    dutyB_percent_RS422M(0) = now_duty[1];
}

void mecanum_TMR_ISR(void) {

    static float now_duty[4] = {}; //現在のduty
    static float up_duty[4] = {}; //現在の必要上昇率
    static float now_deviation[4] = {}; //偏差
    float To_change_necessary_time = 0.0; //目標値までの必要時間(単位時間:10ms)
    uint8_t Max_deviation = 0; //最大偏差の番号
    uint8_t i = 0, t = 0; //for文用
    switch (wheelMode) {
        case wheel_gradual:
        case wheel_gradual_lock:
            if ((wheelMode == wheel_gradual)&&(!safety_lock)) {
                mecanum_control_2016(); /* <アナログスティックの処理> */
            } else {
                for (t = 0; t < 4; t++) {
                    mecanum_Dduty[t] = 0;
                }
            }
            /**************************************************************************/
            /* <偏差の計算> */
            for (t = 0; t < 4; t++) {
                now_deviation[t] = mecanum_Dduty[t] - now_duty[t];
            }
            /* <最大偏差の検索> */
            for (t = 0; t < 4; t++) {
                if ((fabs(now_deviation[t])) > (fabs(now_deviation[Max_deviation]))) {
                    Max_deviation = t;
                }
            }
            /**************************************************************************/
            /* <必要時間の計算> */
            if (now_deviation[Max_deviation] != 0.0) {
                /* <必要時間の計算> */
                To_change_necessary_time = ((To_change_necessary_time_all) * (fabs(now_deviation[Max_deviation]))); //[ms]
                /* <上昇率の計算> */
                for (i = 0; i < 4; i++) {
                    up_duty[i] = ((now_deviation[i]) / (To_change_necessary_time)) * (10.0); //[duty/ms * 10.0]
                }
            } else {
                for (i = 0; i < 4; i++) {
                    up_duty[i] = 0.0; //上昇の停止
                }
            }
            /******************************************************************************/
            /* <dutyの確定 & 次回呼び出しへの準備> */
            for (i = 0; i < 4; i++) {
                /* <dutyの上昇> */
                now_duty[i] = now_duty[i] + up_duty[i];

                /* <上限、下限の設定> */
                if (((up_duty[i] > 0.0) && (now_duty[i] > mecanum_Dduty[i])) ||
                        ((up_duty[i] < 0.0) && (now_duty[i] < mecanum_Dduty[i]))) {
                    up_duty[i] = 0.0;
                    now_duty[i] = mecanum_Dduty[i];
                }
            }
            break;
        case wheel_prompt:
            for (i = 0; i < 4; i++) {
                now_duty[i] = mecanum_Pduty[i];
            }
            break;
    }
    for (i = 0; i < 4; i++) {
        /* <4つのモータへのdutyを確定> */
        setMotor_State((i + 1), (now_duty[i]));
    }
}

void setWheelGradual() {
    wheelMode = wheel_gradual;
}

void setWheelGradualLock() {
    wheelMode = wheel_gradual_lock;
}

void setWheelPrompt() {
    wheelMode = wheel_prompt;
}

typedef enum kssstrc {
    kick_idle,
    kick_pending,
} kickState;
static bool kickreqested = false;

typedef union cyreqstrc {
    uint16_t word;

    struct {
        unsigned FrontUp : 1;
        unsigned FrontDown : 1;
        unsigned BackUp : 1;
        unsigned BackDown : 1;
        unsigned ArmClose : 1;
        unsigned ArmOpen : 1;
    };
} cylinderRequest;
static cylinderRequest cyReq = {0};

typedef union cystastrc {
    uint16_t word;

    struct {
        unsigned Front : 1;
        unsigned Back : 1;
        unsigned Close : 1;
    };
} cylinderState;
static cylinderState cySta = {0};
#define AllDown 3

void FrontCylinderUp() {
    if (!safety_lock)
        cyReq.FrontUp = 1;
}

void FrontCylinderDown() {
    if (!safety_lock)
        cyReq.FrontDown = 1;
}

void BackCylinderUp() {
    if (!safety_lock)
        cyReq.BackUp = 1;
}

void BackCylinderDown() {
    if (!safety_lock)
        cyReq.BackDown = 1;
}

void ArmCylinderOpen() {
    if (!safety_lock)
        cyReq.ArmClose = 1;
}

void ArmCylinderClose() {
    if (!safety_lock)
        cyReq.ArmOpen = 1;
}

void ReqestKick(void) {
    if (!safety_lock)
        kickreqested = true;
}
#define KICK_TIME 500//[ms]
#define TICK_TIME 10//[ms]

void kick_TMR_ISR(void) {
    static kickState kickStateNow = kick_idle;
    static uint16_t tick; //TMR1割り込みで呼ぶので10ms=1tick
    switch (kickStateNow) {
        case kick_idle:
            if (kickreqested) {
                kickStateNow = kick_pending;

                Cylinder3_SetHigh();
                setWheelPrompt();
                setPromptDuty(kickMecanumF, kickMecanumF, kickMecanumB, kickMecanumB, kickCenter, kickCenter);
                tick = 0;
            }
            break;
        case kick_pending:
            if ((++tick) >= (kickTime / TICK_TIME)) {
                setWheelGradualLock();
                Cylinder3_SetLow();
                kickStateNow = kick_idle;
            }
            break;
    }
    kickreqested = false;
}

void Cylinder_SW(void) {
    static uint16_t tick; //TMR1割り込みで呼ぶので10ms=1tick
    static bool FCDowncntdown = false;
    static bool AllDownSWBefore;
    bool AllDownSWNow;

    if (FrontUpSW) {
        FrontCylinderUp();
        FCDowncntdown = false;
    } else if (FrontDownSW) {
        FrontCylinderDown();
        FCDowncntdown = false;
    }
    if (BackUpSW) {
        BackCylinderUp();
    } else if (BackDownSW) {
        BackCylinderDown();
    }
    /* 全ON , OFF */
    if (AllUpSW) {
        FCDowncntdown = false;
        FrontCylinderUp();
        BackCylinderUp();
    } 
    if ((AllDownSWNow = AllDownSW) && !AllDownSWBefore) {
        if(!FCDowncntdown){
            FCDowncntdown = true;
            tick = 0; 
        }
        BackCylinderDown();
    }
    AllDownSWBefore = AllDownSWNow;
    if (FCDowncntdown) {
        if ((tick++) > flontLegDelay / TICK_TIME) {
            FrontCylinderDown();
            FCDowncntdown = false;
        }
    }

    if (ArmCloseSW) {
        ArmCylinderClose();
    } else if (ArmOpenSW) {
        ArmCylinderOpen();
    }

    /**************************************************************************/
    static bool before_state_Cylinder_kick = 0;
    bool now_state_Cylinder_kick;

    now_state_Cylinder_kick = KickSW;
    if ((!before_state_Cylinder_kick) && (now_state_Cylinder_kick)) {
        ReqestKick();
    }
    before_state_Cylinder_kick = now_state_Cylinder_kick;


}

void cylinder_TMR_ISR(void) {
    if (cyReq.FrontUp) {
        Cylinder1_SetLow();
        cySta.Front = 0;
    } else if (cyReq.FrontDown) {
        Cylinder1_SetHigh();
        cySta.Front = 1;
    }
    if (cyReq.BackUp) {
        Cylinder2_SetLow();
        cySta.Back = 0;
    } else if (cyReq.BackDown) {
        Cylinder2_SetHigh();
        cySta.Back = 1;
    }
    if (cyReq.ArmOpen) {
        Cylinder4_SetLow();
        cySta.Close = 0;
    } else if (cyReq.ArmClose) {
        Cylinder4_SetHigh();
        cySta.Close = 1;
    }
    cyReq.word = 0;
}


const char* const StateDiscription[] = {
    "",
    "SeqStart",
    "FrontLgU",
    "BackLgU",
    "KickHabr",
    "ArriveI",
    "ArriveI2",
    "KickIlnd",
    "ArriveNF",
    "FrntLgD1",
    "FrntLgD2",
    "FrntLgD",
    "BackLgD1",
    "BackLgD2",
    "BackLgD3",
    "BackLgD",
    "TheEnd",
};

static SequenceState seqsta = SS_Init;
bool switchpassed = false;
bool goNextSeq = false;
bool goBackSeq = false;
bool dispRenewReq = false;
char* upmes = "Next->";
#if 0
static SequenceState nextseq = SS_Start;

void setSequence(SequenceState next) {
    nextseq = next;
}
#endif


static bool confirmed = true;
static bool confirmEnable;
static bool sequenceEnable;

#define FRAME_TIME 200

void Kondo_TMR_ISR() {
    static int index = 1;
    static int cnt = 0;
    bool UpSWNow = false;
    bool DownSWNow = false;
    static bool UpSWBefore = false;
    static bool DownSWBefore = false;
    if (safety_lock) {
        return;
    }
    cnt++;
    if ((UpSWNow = KondoUpSW)&&(!UpSWBefore)) {
        if (index < KONDO_POS_NUM - 1)index++;
        SetPosition(1, degToPos(kondoPositions[index] + kondoOffset[0]), NULL, false);
        SetPosition(2, degToPos(kondoPositions[index] + kondoOffset[1]), NULL, false);
        cnt = 0;
    } else if ((DownSWNow = KondoDownSW)&&(!DownSWBefore)) {
        if (index > 0)index--;
        SetPosition(1, degToPos(kondoPositions[index] + kondoOffset[0]), NULL, false);
        SetPosition(2, degToPos(kondoPositions[index] + kondoOffset[1]), NULL, false);
        cnt = 0;
    } else if (cnt > 10) {
        SetPosition(1, degToPos(kondoPositions[index] + kondoOffset[0]), NULL, false);
        SetPosition(2, degToPos(kondoPositions[index] + kondoOffset[1]), NULL, false);
        cnt = 0;
    }
    UpSWBefore = UpSWNow;
    DownSWBefore = DownSWNow;
}

static liftState liftnow = lift_init;
static uint8_t indexx = 0;

static bool Manual = false;

void LiftSequence_TMR_ISR() {
    bool UpSWNow = false;
    bool DownSWNow = false;
    static bool UpSWBefore = false;
    static bool DownSWBefore = false;
    bool UpManuSWNow = false;
    bool DownManuSWNow = false;
    static bool UpManuSWBefore = false;
    static bool DownManuSWBefore = false;
    bool SetSWNow = false;
    static bool SetSWBefore = false;

    if (safety_lock) {
        setLiftDuty(0);
        return;
    }
#if 0
    if ((UpSWNow = LiftUpSW)) {
        setLiftDuty(liftDutiesUp);
    } else if ((DownSWNow = LiftDownSW)) {
        setLiftDuty(liftDutiesDown);
    } else {
        setLiftDuty(0);
    }
#else
    switch (liftnow) {
        case lift_init:
            liftnow = lift_searchOrigin1;
            setLiftDuty(liftDutiesDown);
            initLiftRawPWM();
        case lift_searchOrigin1:
            if (SW_LiftOrigin) {
                liftnow = lift_searchOrigin2;
                setLiftDuty(liftDutiesUp);
            }
            break;
        case lift_searchOrigin2:
            if (!SW_LiftOrigin) {
                liftnow = lift_normal;
                setLiftPosition(liftPositions[indexx = 0]);
                initLiftFeedBack();
                Manual = false;
            }
            break;
        case lift_normal:
            if ((SetSWNow = LiftSetSW)&&(!SetSWBefore)) {
                setLiftPosition(liftPositions[indexx]);
                Manual = false;
            } else
                if ((UpSWNow = LiftUpSW)&&(!UpSWBefore)) {
                if (indexx < LIFT_POS_NUM - 1)indexx++;
            } else if ((DownSWNow = LiftDownSW)&&(!DownSWBefore)) {
                if (indexx > 0)indexx--;
            } else if ((UpManuSWNow = LiftManualUpSW)) {
                setLiftPosition(liftMax);
                Manual = true;
            } else if ((DownManuSWNow = LiftManualDownSW)) {
                setLiftPosition(liftMin);
                Manual = true;
            } else if (Manual) {
                setLiftPosition(getNowPosition());
                Manual = false;
            }

            UpSWBefore = UpSWNow;
            DownSWBefore = DownSWNow;
            UpManuSWBefore = UpManuSWNow;
            DownManuSWBefore = DownManuSWNow;
            SetSWBefore = SetSWNow;
            break;
    }
#endif
}

void Sequence_TMR_ISR() {
    limit limitNow;
    static uint16_t cnt = 0;

    bool confirmSWNow = false;
    static bool confirmSWBefore = false;

    bool SequenceGoSWNow = false;
    static bool SequenceGoSWBefore = false;
    bool SequenceBackSWNow = false;
    static bool SequenceBackSWBefore = false;
    bool SequenceIndex1SWNow = false;
    static bool SequenceIndex1SWBefore = false;
    bool SequenceIndex2SWNow = false;
    static bool SequenceIndex2SWBefore = false;
    char str[30] = {};
    limitNow = sampling_filter_limit();
    if (safety_lock)return;
    if (wheelMode == wheel_gradual_lock) {
        if (limitNow.Landing) {
            wheelMode = wheel_gradual;
        }
    }
    confirmEnable = ConfirmEnableSW;
    sequenceEnable = SequenceEnableSW;
    SequenceGoSWNow = SequenceGoSW;
    SequenceBackSWNow = SequenceBackSW;
    SequenceIndex1SWNow = SequenceIndex1SW;
    SequenceIndex2SWNow = SequenceIndex2SW;
    if ((++cnt) >= (FRAME_TIME / TICK_TIME)) {
        cnt = 0;
        dispRenewReq = true;
    }
    if (sequenceEnable) {
        if (SequenceFunctionSW) {
            if (SequenceGoSWNow && !SequenceGoSWBefore) {
                goNextSeq = true;
            }
            if (SequenceBackSWNow && !SequenceBackSWBefore) {
                goBackSeq = true;
            }
            if (SequenceIndex1SWNow && !SequenceIndex1SWBefore) {
                seqsta = SS_Start;
                switchpassed = false;
                dispRenewReq = true;
            }
            if (SequenceIndex2SWNow && !SequenceIndex2SWBefore) {
                seqsta = SS_KickFromIsland;
                switchpassed = false;
                dispRenewReq = true;
            }
        }
        SequenceGoSWBefore = SequenceGoSWNow;
        SequenceBackSWBefore = SequenceBackSWNow;
        SequenceIndex1SWBefore = SequenceIndex1SWNow;
        SequenceIndex2SWBefore = SequenceIndex2SWNow;
        if (!switchpassed) {
            switch (seqsta) {
                case SS_Init:
                    switchpassed = true;
                    //setSequence(SS_Start);
                    break;
                case SS_Start:
                    if (cySta.word == AllDown) {
                        switchpassed = true;
                        //setSequence(SS_FrontLegUp);
                    }
                    break;
                case SS_FrontLegUp:
                    if (limitNow.FrontUp) {
                        switchpassed = true;
                        //setSequence(SS_BackLegUp);
                    }
                    break;
                case SS_BackLegUp:
                    if (limitNow.BackUp) {
                        switchpassed = true;
                        //setSequence(SS_KickFromHarbor);
                    }
                    break;
                case SS_KickFromHarbor:
                    if (limitNow.Kick) {
                        switchpassed = true;
                        //setSequence(SS_ArrivalAtIsland);
                    }
                    break;
                case SS_ArrivalAtIsland:
                    if (limitNow.Landing) {
                        switchpassed = true;
                        //setSequence(SS_ArrivalAtIsland2);
                    }
                    break;
                case SS_ArrivalAtIsland2:
                    if (limitNow.FrontUp&&!limitNow.FrontDown && limitNow.BackUp&&!limitNow.BackDown) {
                        switchpassed = true;
                        //setSequence(SS_KickFromIsland);
                    }
                    break;
                case SS_KickFromIsland:
                    if (limitNow.Kick) {
                        switchpassed = true;
                        //setSequence(SS_ArrivalAtNewFrontier);
                    }
                    break;
                case SS_ArrivalAtNewFrontier:
                    if (limitNow.Landing) {
                        switchpassed = true;
                        //setSequence(SS_FrontLegDown);
                    }
                    break;
                case SS_FrontLegDownPrev1:
                    if (!limitNow.Landing) {
                        switchpassed = true;
                        //setSequence(SS_BackLegDown);
                    }
                    break;
                case SS_FrontLegDownPrev2:
                    if (!limitNow.FrontDown) {
                        switchpassed = true;
                        //setSequence(SS_BackLegDown);
                    }
                    break;
                case SS_FrontLegDown:
                    if (limitNow.FrontDown) {
                        switchpassed = true;
                        //setSequence(SS_BackLegDown);
                    }
                    break;
                case SS_BackLegDownPrev1:
                    if (!limitNow.Kick) {
                        switchpassed = true;
                        //setSequence(SS_End);
                    }
                    break;
                case SS_BackLegDownPrev2:
                    if (limitNow.Kick) {
                        switchpassed = true;
                        //setSequence(SS_End);
                    }
                    break;
                case SS_BackLegDownPrev3:
                    if (!limitNow.BackDown) {
                        switchpassed = true;
                        //setSequence(SS_End);
                    }
                    break;
                case SS_BackLegDown:
                    if (limitNow.BackDown) {
                        switchpassed = true;
                        //setSequence(SS_End);
                    }
                    break;
                case SS_End:
                    break;
            }

            if (confirmEnable && switchpassed) {
                dispRenewReq = true;
            }

        }

        if (seqsta != SS_Init) {
            confirmSWNow = SequenceYesSW;
            confirmed = (!confirmSWBefore) && confirmSWNow;
            confirmSWBefore = confirmSWNow;
        }

        if ((confirmed || !confirmEnable) && switchpassed) {
            switch (seqsta) {
                case SS_Init:
                    clearScreen_Ctrl();
                    break;
                case SS_Start:
                    break;
                case SS_FrontLegUp:
                    FrontCylinderUp();
                    break;
                case SS_BackLegUp:
                    BackCylinderUp();
                    break;
                case SS_KickFromHarbor:
                    ReqestKick();
                    break;
                case SS_ArrivalAtIsland:
                    break;
                case SS_ArrivalAtIsland2:
                    break;
                case SS_KickFromIsland:
                    ReqestKick();
                    break;
                case SS_ArrivalAtNewFrontier:
                    break;
                case SS_FrontLegDownPrev1:
                    break;
                case SS_FrontLegDownPrev2:
                    break;
                case SS_FrontLegDown:
                    FrontCylinderDown();
                    break;
                case SS_BackLegDownPrev1:
                    break;
                case SS_BackLegDownPrev2:
                    break;
                case SS_BackLegDownPrev3:
                    break;
                case SS_BackLegDown:
                    BackCylinderDown();
                    break;
                case SS_End:
                    break;
            }
            goNextSeq = true;
        }
        if (goNextSeq) {
            goNextSeq = false;
            if (seqsta < SS_End)
                seqsta++;
            switchpassed = false;
            dispRenewReq = true;
        }
        if (goBackSeq) {
            goBackSeq = false;
            if (seqsta > SS_Start)
                seqsta--;
            switchpassed = false;
            dispRenewReq = true;
        }
    }

    if (dispRenewReq) {
        dispRenewReq = false;
        if (sequenceEnable) {
            if (switchpassed) {
                upmes = "Go!";
            } else {
                upmes = "Next";
            }
        } else {
            upmes = "SOFF";
        }

        clearScreen_Ctrl();
        sprintf(str, "%-4.4s", upmes);
        set_next_mes_FIFO(0, 0, (uint8_t*) str);
        sprintf(str, "%8d", indexx);
        set_next_mes_FIFO(0, 4, (uint8_t*) ((Manual) ? "    " : str));
        sprintf(str, "%-4.4s", wheelMes[wheelMode]);
        set_next_mes_FIFO(0, 12, (uint8_t*) str);
        sprintf(str, "%-8.8s", (sequenceEnable) ? StateDiscription[seqsta] : "");
        set_next_mes_FIFO(1, 0, (uint8_t*) str);
        sprintf(str, "%4.1f", (double) getNowPosition());
        set_next_mes_FIFO(1, 8, (uint8_t*) str);
        sprintf(str, "%-4.4s", (sequenceEnable) ? ((confirmEnable) ? "CON" : "COFF") : "");
        set_next_mes_FIFO(1, 12, (uint8_t*) str);
    }
}

void mecanum_TMR_ISR_Auto(void) {
    LiftSequence_TMR_ISR();
    Kondo_TMR_ISR();
    Sequence_TMR_ISR();
    if (wheelMode == wheel_gradual_lock) {
        if (UnlockSW) {
            wheelMode = wheel_gradual;
        }
    }
    kick_TMR_ISR();
    cylinder_TMR_ISR();
    mecanum_TMR_ISR();
    center_TMR_ISR();
}

#define once_OFF_2016   0
#define once_ON_2016    1
#define once_idle_2016  2

#define MAX_CNT_limit (5)//10ms * n

limit sampling_filter_limit() {
    //    char str[20];
    static limit state; // = 0;
    static CNT_4bit CNT_ISR[3] = {};

    /******************************/
    if (SW_FR_or_FL_UP != state.Landing) {
        CNT_ISR[0].CNT1++;
        if (CNT_ISR[0].CNT1 >= MAX_CNT_limit) {
            CNT_ISR[0].CNT1 = 0;
            state.Landing = SW_FR_or_FL_UP;
        }
    } else {
        CNT_ISR[0].CNT1 = 0;
    }
    /******************************/
    if (SW_FR_and_FL_UP != state.FrontUp) {
        CNT_ISR[0].CNT2++;
        if (CNT_ISR[0].CNT2 >= MAX_CNT_limit) {
            CNT_ISR[0].CNT2 = 0;
            state.FrontUp = SW_FR_and_FL_UP;
        }
    } else {
        CNT_ISR[0].CNT2 = 0;
    }
    /******************************/
    if (SW_FR_and_FL_DOWN != state.FrontDown) {
        CNT_ISR[1].CNT1++;
        if (CNT_ISR[1].CNT1 >= MAX_CNT_limit) {
            CNT_ISR[1].CNT1 = 0;
            state.FrontDown = SW_FR_and_FL_DOWN;
        }
    } else {
        CNT_ISR[1].CNT1 = 0;
    }
    /******************************/
    if (SW_BR_and_BL_UP != state.BackUp) {
        CNT_ISR[1].CNT2++;
        if (CNT_ISR[1].CNT2 >= MAX_CNT_limit) {
            CNT_ISR[1].CNT2 = 0;
            state.BackUp = SW_BR_and_BL_UP;
        }
    } else {
        CNT_ISR[1].CNT2 = 0;
    }
    /******************************/
    if (SW_BR_and_BL_DOWN != state.BackDown) {
        CNT_ISR[2].CNT1++;
        if (CNT_ISR[2].CNT1 >= MAX_CNT_limit) {
            CNT_ISR[2].CNT1 = 0;
            state.BackDown = SW_BR_and_BL_DOWN;
        }
    } else {
        CNT_ISR[2].CNT1 = 0;
    }
    /******************************/
    if (SW_kick_R_and_L != state.Kick) {
        CNT_ISR[2].CNT2++;
        if (CNT_ISR[2].CNT2 >= MAX_CNT_limit) {
            CNT_ISR[2].CNT2 = 0;
            state.Kick = SW_kick_R_and_L;
        }
    } else {
        CNT_ISR[2].CNT2 = 0;
    }
    //sprintf(str,"%2x:%1d%1d%1d%1d%1d%1d\r\n",state.byte,CNT_ISR[0].CNT1,CNT_ISR[0].CNT2,CNT_ISR[1].CNT1,CNT_ISR[1].CNT2,CNT_ISR[2].CNT1,CNT_ISR[2].CNT2);
    //DEBUG_puts(str);
    return state;
}



/* 各割り込み関数 */

static uint16_t times_for_timeout = 0;

//static uint16_t times_for_timeout = 0;

/* 10ms割り込み: メカナム,コントローラタイムアウト, */
void __attribute__((interrupt, auto_psv)) _T1Interrupt(void) {
    static uint8_t times_for_LED = 0;
    static uint8_t times_for_OLED = 0;
    static int help_me = 0;
    // char str[30];
    _T1IF = 0;
    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    times_for_LED++;
    times_for_OLED++;
    if ((++help_me) > 100) {
        help_me = 0;
        LED_debug_Toggle();
    }
    /*メカナム*/
    //mecanum_TMR_ISR();
    Cylinder_SW();
    mecanum_TMR_ISR_Auto();
    //mecanum_control_2016();

    /*limitスイッチ*/
    //LimitSwitch_check(); //limitSW

    /* <コントローラの表示を初期化> */
    /* <各通信状況LED OFF> */
    if (times_for_LED == 30) {
        check_LED_Controller_SetLow();
        check_LED_RS422_SetLow();
        //sprintf(str,"%d:%d:%d:%d:%d\r\n",now_data_exist_Ctrl,now_space_exist_Ctrl,now_transmit_Ctrl,_U2TXIE,_U2TXIF);
        //DEBUG_puts(str);
        times_for_LED = 0;
    }

    /* コントローラ用タイムアウト*/
    times_for_timeout++;
    if (times_for_timeout > 100) {
        init_MOTOR();
        times_for_timeout = 0;
        init_controller_data();

    }

}

/*UART1 : もん通信*/
void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void) {

    _U1RXIF = 0;
    check_LED_RS422_SetHigh();
    rx_ISR_RS422();

}

void __attribute__((interrupt, auto_psv)) _U1TXInterrupt(void) {

    _U1TXIF = 0;
    __delay_us(10);
    tx_ISR_RS422();

}

void __attribute__((interrupt, auto_psv)) _T5Interrupt(void) {

    _T5IF = 0;
    tmr_ISR_RS422();

}

/*UART2 : コントローラ*/
void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {

    _U2RXIF = 0;

    if (Controler_checker_ISR(2) == true) {
        check_LED_Controller_SetHigh();
        times_for_timeout = 0;
    } else {
        check_LED_Controller_SetLow();
    }

}

void __attribute__((interrupt, auto_psv)) _U2TXInterrupt(void) {

    FIFO_send_Controller_ISR();

}

/**/
void __attribute__((interrupt, auto_psv)) _U3RXInterrupt(void) {

    //書き込みはもん構造体に準拠した形
    // "device_id" "data_id" "length" "data1 data2 ..."

    // 注意事項
    // datax は8bit
    // lengthを 0x99 でEEPROM内すべて0へ
    // "OK"の返信で 1word 書き込み完了
    // 現在ASCIIに非対応

    rx_ISR_toEEPROM(); //PCからEEPROMへの書き込み用
    _U3RXIF = 0;

}

void __attribute__((interrupt, auto_psv)) _U3TXInterrupt(void) {

    // FIFO_send_Controller_ISR();
    _U3TXIF = 0;
}

/* KONDO用 */
void __attribute__((interrupt, auto_psv)) _U4RXInterrupt(void) {

    _U4RXIF = 0;
    KondoUARTRxTask();

}

void __attribute__((interrupt, auto_psv)) _U4TXInterrupt(void) {

    _U4TXIF = 0;

}

void __attribute__((interrupt, auto_psv)) _T3Interrupt(void) {

    _T3IF = 0;
    KondoTimeOutTask();

}

/* I2C : EEPROM書き込み */
#if 0

void __attribute__((interrupt, auto_psv)) _MI2C1Interrupt(void) {

    //書き込みはもん構造体に準拠した形

    I2CM_ISR_toEEPROM();

    return;

}
#endif
//文字列送信

void DEBUG_puts(const char* string) {
    do {
        UART3_Write(*(string));
    } while (*(string++) != '\0');

    return;

}
/**************************************************************************/

/* <コントローラ送信> */
void putUART2_2016(uint8_t data) {
    _U2TXIF = 0;
    putUART2(data);
}

void stop_TX_UART2_2016() {
    _U2TXIE = 0;
}

void start_TX_UART2_2016() {
    _U2TXIE = 1;
}

/*<デバッグ送信>*/
void putUART3_2016(uint8_t data) {
    putUART3(data);
    _U3TXIE = 1;
}

void stop_TX_UART3_2016() {
    _U3TXIE = 0;
}

/******************************************************************************/

/* KONDO.h用 */
void KondoPutByte(uint8_t data) {
    _U4TXIF = 0;
    U4TXREG = data;
}//1バイト送信関数

uint8_t KondoGetByte() {
    uint8_t recieve_data_KONDO = U4RXREG;
    _U4RXIF = 0;
    return recieve_data_KONDO;
}//1バイト受信関数

bool KondoGettable() {
    return (U4STAbits.URXDA == 1);
} //有効なデータがあるかどうか

void KondoSetRXIE(bool en) {
    _U4RXIE = en;
} //受信割り込み許可設定

void KondoClearRXIF() {
    _U4RXIF = 0;
} //受信割り込みフラグクリア

void KondoStartTimer() {

    setdelay(3, micro_mode_TMR, (((1.0 / UART4_bps) * 100.0) * 1000000.0), ON);

} //タイムアウトタイマー初期化&スタート

void KondoStopTimer() {
    T3CONbits.TON = 0;
    _T3IF = 0;
} //タイムアウトタイマー停止

/*コンフィグ用**/


void putsDisp(uint8_t li, uint8_t col, const char* string) {
    set_next_mes_FIFO(li, col, (uint8_t *) string);
}

void clearDisp() {
    clearScreen_Ctrl();
}

void wait_ms(uint16_t time) {
    while (time > 10) {
        __delay_ms(10);
        time -= 10;
    }
    __delay_ms(time);
}

ConfigButton getButtons() {
    ConfigButton cb;
    cb.end = ((ConfigEndSW || (!ConfigEnableSW)) ? 1 : 0);
    cb.down = (ConfigDownSW ? 1 : 0);
    cb.up = (ConfigUpSW ? 1 : 0);
    cb.back = (ConfigBackSW ? 1 : 0);
    cb.next = (ConfigNextSW ? 1 : 0);
    return cb;
}

void safetyLock(bool en) {
    safety_lock = en;
}

void initLiftRawPWM() {
    change_PWM_STAP_MODE(1);
}

void initLiftFeedBack() {
    change_Position_STAP_MODE(1);
}

float getNowPosition() {
    return now_position_A_RS422M(1);
}

void setLiftDuty(float duty) {
    dutyA_percent_RS422M(1) = duty;
}

void setLiftSpeed(float spd) {
    Dposition_A_up_speed_RS422M(1) = spd;
}

void setLiftPosition(float pos) {
    Dposition_A_RS422M(1) = pos;
}

void setPIDcoefficients(float Kp, float Ki, float Kd) {
    P_A_gein_RS422M(1) = Kp;
    I_A_gein_RS422M(1) = Ki;
    D_A_gein_RS422M(1) = Kd;
}