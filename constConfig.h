/* 
 * File:   constConfig.h
 * Author: Robo6
 *
 * Created on October 27, 2016, 3:56 PM
 */

#ifndef CONSTCONFIG_H
#define	CONSTCONFIG_H
#include <stdint.h>
#ifdef	__cplusplus
extern "C" {
#endif
    typedef struct paramstrc{
        float* address;
        float upRate;
        float max;
        float min;
        char* name;
    } Parameter;

    /**メインループ前に入れてほしい関数
     * param:パラメータの配列
     * n:配列の長さ
     * 
     実装はconstConfig.cでやるよ
     例
     *
     float maxDuty[2];
     float servoDegree[2];
     
     Parameter param[] = {   
        {&maxDuty[0],0.01,1.0,0.0},
        {&maxDuty[1],0.01,1.0,0.0},
        {&servoDegree[0],1.0,270.0,0.0},
        {&servoDegree[1],1.0,270.0,0.0},
     }
     #define PARAM_NUM (sizeof(param)/sizeof(param[0]))//要素数
     　 void main(void){
            SYSTEM_initialize();
            Global_Interrupt_Disable();//余計な割り込みを禁止
            constConfig(param,PARAM_NUM);
            Global_Interrupt_Enable();//余計な割り込みを禁止していたので有効化
            while(1){
                Main_Task();
            }
        }
     */
    void constConfig(Parameter* param, uint16_t n);


    /*外部で定義してほしい関数*/
    /*指定位置に文字を表示する関数
     * li:行
     * col:列
     * string:文字列
     * 実装例:
@code
    void putsDisp(uint8_t li, uint8_t col, const char* string) {
        while (UART2_TransmitBufferIsFull());
        UART2_Write((uint8_t) ('\x11'));
        while (UART2_TransmitBufferIsFull());
        UART2_Write(li);
        while (UART2_TransmitBufferIsFull());
        UART2_Write((uint8_t) ('\x12'));
        while (UART2_TransmitBufferIsFull());
        UART2_Write(col);
        while (*(string) != '\0')
            while (UART2_TransmitBufferIsFull());
            UART2_Write((uint8_t) (*(string++)));
        };
    }
     * 
     */
    void putsDisp(uint8_t li, uint8_t col, const char* string);
    /*文字を全部消す関数
     * 実装例:
@code
    void clearDisp(void) {
        while (UART2_TransmitBufferIsFull());
        UART2_Write((uint8_t) ('\0'));
    }
     * 
     */
    void clearDisp();
#define CONF_BTN_NEXT 0x01
#define CONF_BTN_BACK 0x02
#define CONF_BTN_UP 0x04
#define CONF_BTN_DOWN 0x08
#define CONF_BTN_END 0x10
    typedef union buttonUnion {
        uint16_t word;

        struct {
            unsigned next : 1;
            unsigned back : 1;
            unsigned up : 1;
            unsigned down : 1;
            unsigned end : 1;
        };
    } ConfigButton;
    /*外部で定義してほしい関数
     * コントローラ及び基板のスイッチの状態の取得
     * 返り値 16bitの構造体
     * 15?5bit:0
     * 4bit:基板スイッチ
     * 3bit:値下げるボタン
     * 2bit:値上げるボタン
     * 1bit:項目戻るボタン
     * 0bit:項目進むボタン
     */
    ConfigButton getButtons(void);

    /*外部で定義してほしい関数
     * time[ms]間待ってくれるやつ
     * 例
        void wait_ms(uint8_t time){
            __delay_ms(time);
        }
     */
    void wait_ms(uint16_t time);
#ifdef	__cplusplus
}
#endif

#endif	/* CONSTCONFIG_H */

