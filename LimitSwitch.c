#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>
#include "LimitSwitch.h"
#include "PIC24FJxxxGC006_head.h"
#include "function_2016_japancap.h"

void LimitSwitch_check() {
//    limit state = sampling_filter_limit();
#if 0
    if (limit_PORT1 == 1) {
        putUART3('0');
    } else {

    }
    if (limit_PORT2 == 1) {
        putUART3('1');
    } else {

    }
    if (limit_PORT3 == 1) {
        putUART3('2');
    } else {

    }
    if (limit_PORT4 == 1) {
        putUART3('3');
    } else {

    }
    if (limit_PORT5 == 1) {
        putUART3('4');
    } else {

    }
    if (limit_PORT6 == 1) {
        putUART3('5');
    } else {

    }
    if (limit_PORT7 == 1) {
        putUART3('6');
    } else {

    }
    if (limit_PORT8 == 1) {
        putUART3('7');
    } else {

    }
    if (limit_PORT9 == 1) {
        putUART3('8');
    } else {

    }
    if (limit_PORT10 == 1) {
        putUART3('9');
    } else {

    }
#endif
#if 0
    static uint8_t times = 0;
    uint8_t str[30] = {};
    times++;
    if (times == 2) {
        sprintf(str, "%1d %1d %1d %1d %1d %1d", state.Landing, state.FrontUp, state.FrontDown, state.BackUp, state.BackDown, state.Kick);
        set_next_mes_FIFO(0, 0, str);
        times = 0;
    }
#endif
}
