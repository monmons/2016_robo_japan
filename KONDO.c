#include "KONDO.h"
#include "stdlib.h"
#if KONDO_EEPROM_USE
#define TX_STR_LENGTH 128
#define RX_STR_LENGTH 128
#else
#define TX_STR_LENGTH 16
#endif


#define LAST_READ 0
#define LAST_WRITTEN 1

#define FREE_POS_VAL 0

typedef enum {
    idle,
    transmit,
    recieve,
} kondoComState;

typedef struct kondostrct {
    uint8_t transmitData[TX_STR_LENGTH];
    uint8_t transmitNum;
    uint8_t recieveNum;
    uint8_t* resultPtr;
    KONDO_COM_RESULT* isResultReady;
} kondoMes;

static kondoMes txbuf[KONDO_BUF_SIZE]; //送信メッセージバッファ
static kondoMes* txHead = txbuf; //送信メッセージバッファ先頭,すなわち一番古いメッセージが入っていて、今処理されているところ
static kondoMes* txTail = txbuf; //送信メッセージバッファ最後尾,すなわち次のSendMes関数で最新のメッセージが入る所
static bool txlast = LAST_READ; //最後の動作は読みこみか書きこみか
#define txIsFull ((txlast == LAST_WRITTEN)&&(txTail == txHead))//先頭と後尾が一致かつ最後に書き込み->バッファ満杯
#define txIsEmpty ((txlast == LAST_READ)&&(txTail == txHead))//先頭と後尾が一致かつ最後に読み込み->バッファ空

static uint16_t nowComNum = 0;
static kondoComState state = idle;
static uint8_t index = 0;

void KondoUARTRxTask() {
    uint8_t recievedData;
    KondoClearRXIF();
    while (KondoGettable()) {
        recievedData = KondoGetByte();
        if ((state == transmit) && (recievedData != txHead->transmitData[nowComNum])) {
            continue;
        }
        KondoStopTimer();
        nowComNum++;
        switch (state) {
            case transmit:
                if (nowComNum < txHead->transmitNum) {
                    KondoPutByte(txHead->transmitData[nowComNum]);
                    KondoStartTimer();
                    break;
                } else {
                    state = recieve;
                    index = 0;
                }
            case recieve:
                if (nowComNum < txHead->transmitNum + txHead->recieveNum) {
                    if (txHead->resultPtr != NULL) {
                        txHead->resultPtr[index++] = recievedData;
                    }
                    KondoStartTimer();
                } else {
                    if (txHead->isResultReady != NULL) {
                        *(txHead->isResultReady) = successful;
                    }
                    /*バッファの後尾を進める*/
                    if (++txHead >= txbuf + KONDO_BUF_SIZE)txHead = txbuf;
                    txlast = LAST_READ; //バッファのデータが取り出された
                    nowComNum = 0;
                    if (!txIsEmpty) {//まだ送っていないメッセージがあるなら
                        KondoPutByte(txHead->transmitData[nowComNum]);
                        KondoStartTimer();
                        state = transmit;
                    } else {//もう送るメッセージはない
                        state = idle; //アイドル状態へ
                    }
                }
                break;
            default:
                break;
        }
    }


}

void KondoTimeOutTask() {
    KondoStopTimer();
    if (txHead->isResultReady != NULL) {
        *(txHead->isResultReady) = failed;
    }
    /*バッファの後尾を進める*/
    if (++txHead >= txbuf + KONDO_BUF_SIZE)txHead = txbuf;
    txlast = LAST_READ; //バッファのデータが取り出された
    nowComNum = 0;
    if (!txIsEmpty) {//まだ送っていないメッセージがあるなら
        KondoPutByte(txHead->transmitData[nowComNum]);
        KondoStartTimer();
        state = transmit;
    } else {//もう送るメッセージはない
        state = idle; //アイドル状態へ
    }
}

bool SendKondoMes(uint8_t* txData, uint8_t txLength, uint8_t* rxData, uint8_t rxLength, KONDO_COM_RESULT* stat) {
    if (txIsFull) {//既にバッファが満杯、もう入らない
        /*
         * <TODO>エラー発生
         */
        return false;
    }
    KondoSetRXIE(false);
    /*
     * バッファの後ろにメッセージのデータを代入
     */
    int i;
    for (i = 0; i < txLength; i++) {
        txTail->transmitData[i] = txData[i];
    }
    txTail->transmitNum = txLength;
    txTail->resultPtr = rxData;
    txTail->recieveNum = rxLength;
    txTail->isResultReady = stat;
    if (txTail->isResultReady != NULL) {
        *(txTail->isResultReady) = pending;
    }
    /*
     * バッファの先頭を進める
     * リングバッファなので、配列の最後に行ったら、次は最初に戻ってくる
     */
    if (++txTail >= txbuf + KONDO_BUF_SIZE)txTail = txbuf;

    /*
     * バッファに書き込んだので txlast = LAST_WRITTEN
     */
    txlast = LAST_WRITTEN;

    if (state == idle) {
        nowComNum = 0;
        KondoPutByte(txHead->transmitData[nowComNum]);
        KondoStartTimer();
        state = transmit;
    }
    KondoSetRXIE(true);

    return true;
}
/*
            if (kState[KONDO_1] != KONDO_FREE) {
                __builtin_disi(0x3FFF);
                SetPosition(kondoID[KONDO_1], degToPos(kondoPosition[KONDO_1][kState[KONDO_1]]), &val, true);
                __builtin_disi(0x0000);
            } else {
                ServoFree(kondoID[KONDO_1], &val, true);
            }
 */
bool SetPosition(uint8_t id, uint16_t p, uint16_t* val, bool wait) {
    uint8_t txDat[3];
    uint8_t rxDat[3];
    KONDO_COM_RESULT tempstat;
    txDat[0] = SET_POS | id;
    txDat[1] = ((p >> 7)&0x7f);
    txDat[2] = (p & 0x7f);
    if ((val == NULL) && (!wait)) {
        return SendKondoMes(txDat, 3, NULL, 3, NULL);
    } else {
        if (!SendKondoMes(txDat, 3, rxDat, 3, &tempstat))return false;
        while (tempstat == pending);
        if (tempstat == failed)return false;
        if (val != NULL) {
            *val = (((rxDat[2]&0x7f) << 7) | (rxDat[1]&0x7f));
        }
        return true;
    }
}

bool ServoFree(uint8_t id, uint16_t* val, bool wait) {
    return SetPosition(id, FREE_POS_VAL, val, wait);
}
bool GetParameter(uint8_t id, uint8_t sc, uint8_t* val, bool wait) {
    uint8_t txDat[2];
    uint8_t rxDat[3];
    KONDO_COM_RESULT tempstat;
    txDat[0] = GET_PRM | id;
    txDat[1] = sc;
    if (val == NULL && !wait) {
        return SendKondoMes(txDat, 2, NULL, 3, NULL);
    } else {
        if (!SendKondoMes(txDat, 2, rxDat, 3, &tempstat))return false;
        while (tempstat == pending);
        if (tempstat == failed)return false;
        if (val != NULL) {
            *val = rxDat[2];
        }
        return true;
    }
}

bool SetParameter(uint8_t id, uint8_t sc, uint8_t val, bool wait) {
    uint8_t txDat[3];
    uint8_t rxDat[3];
    KONDO_COM_RESULT tempstat;
    txDat[0] = SET_PRM | id;
    txDat[1] = sc;
    txDat[2] = val;
    if (!wait) {
        return SendKondoMes(txDat, 3, NULL, 3, NULL);
    } else {
        if (!SendKondoMes(txDat, 3, rxDat, 3, &tempstat))return false;
        while (tempstat == pending);
        if (tempstat == failed)return false;
        return true;
    }
}
#if KONDO_EEPROM_USE
//branchできてるかなー
bool GetID(bool wait) {

}

bool SetID(uint8_t id, uint8_t val, bool wait) {

}

bool GetEEPROM(uint8_t id) {

}

bool SetEEPROM(uint8_t id) {

}
#endif
