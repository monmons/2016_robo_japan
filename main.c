/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.16
        Device            :  PIC24FJ64GC006
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC16 1.26
        MPLAB             :  MPLAB X 3.20
 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
 */

#include "mcc_generated_files/mcc.h"
#include "function_2016_japancap.h"//割り込み関数も入ってます
#include "constConfig.h"
#include "ROM.h"
#include "KONDO.h"
/*
                         Main application
 */
float kondoOffset[2];
float wheelsLimits[3];
float liftPositions[LIFT_POS_NUM];
float liftMax;
float liftMin;
float kondoPositions[KONDO_POS_NUM];
float liftDutiesDown;
float liftDutiesUp;
float liftDutiesDownOrigin;
float liftDutiesUpOrigin;
float liftSpeed;
float kickMecanumF;
float kickMecanumB;
float kickCenter;
float kickTime;
float Kp;
float Ki;
float Kd;
float flontLegDelay;
Parameter param[] = {
    {&wheelsLimits[wheel_fast], 0.01, 1.0, 0.0, "MaxFast"},
    {&wheelsLimits[wheel_normal], 0.01, 1.0, 0.0, "MaxNormal"},
    {&wheelsLimits[wheel_slow], 0.01, 1.0, 0.0, "MaxSlow"},
    {&kickMecanumF, 0.01, 1.0, -1.0, "KickFront"},
    {&kickCenter, 0.01, 1.0, -1.0, "KickCenter"},
    {&kickMecanumB, 0.01, 1.0, -1.0, "KickBack"},
    {&kickTime, 10.0, 500.0, 0.0, "KickTime[ms]"},
    {&liftDutiesDown, 0.01, 1.0, -1.0, "LiftDown"},
    {&liftDutiesUp, 0.01, 1.0, -1.0, "LiftUp"},
    {&Kp, 0.1, 10.0, -10.0, "Kp"},
    {&Ki, 0.1, 10.0, -10.0, "Ki"},
    {&Kd, 0.1, 10.0, -10.0, "Kd"},
    {&liftPositions[0], 0.1, 100.0, 0.0, "Lift1"},
    {&liftPositions[1], 0.1, 100.0, 0.0, "Lift2"},
    {&liftPositions[2], 0.1, 100.0, 0.0, "Lift3"},
    {&kondoPositions[0], 1.0, 270.0, 0.0, "Kondo1"},
    {&kondoPositions[1], 1.0, 270.0, 0.0, "Kondo2"},
    {&kondoPositions[2], 1.0, 270.0, 0.0, "Kondo3"},
    {&kondoOffset[0], 1.0, 10.0, -10.0, "KOffsetL"},
    {&kondoOffset[1], 1.0, 10.0, -10.0, "KOffsetR"},
    {&liftPositions[3], 0.1, 100.0, 0.0, "Lift4"},
    {&liftPositions[4], 0.1, 100.0, 0.0, "Lift5"},
    {&liftMax, 0.1, 100.0, 0.0, "LiftMax"},
    {&liftMin, 0.1, 100.0, 0.0, "LiftMin"},
    {&liftSpeed, 0.1, 10.0, 0.0, "LiftSpeed"},
    {&flontLegDelay,50.0, 1000.0, 0.0, "FlontDelay"},
};
#define PARAM_NUM (sizeof(param)/sizeof(param[0]))//要素数
uint8_t ch;
char str[30];

int main(void) {
    // initialize the device
    SYSTEM_Initialize(); //FCY:16MHz
    init_controller_data(); //コントローラdataの初期化
    init_MOTOR(); //モータ４つの状態をブレーキ状態へ
    /**************************************************************************/
    //割り込みについては各モジュールのsetup関数にて有効かする

    /*TEST PROGRAM*/
    //TEST0
#if 0
    while (1) {
        putUART3(0xAA);
        __delay_ms(1);
    }
#endif
    //TEST1(モータの駆動)
#if 0
    setMotor_State(1, -0.9); //モータ1を50%で駆動
    setMotor_State(2, -0.98); //モータ2を80%で駆動
    while (1);
#endif
    //TEST2(コントローラ受信の可否)
#if 0
    setupUART();
    while (1);
#endif
    //TEST3(リミットスイッチの入力)
#if 0
    setdelay(1, milli_mode_TMR, 10.0, ON); //TMR1 , ミリ , 10.0 ,割り込み可
    while (1);
#endif
    //TEST4(RS422の可否)
#if 0
    reset_All_RS422();
    init_PWM_RS422(0); //device_id : 0  を単純PWMモード
    dutyA_percent_RS422M(0) = 1.0;
    dutyB_percent_RS422M(0) = 1.0;
    restart_RS422();
    setupUART();
    while (1) {
        dutyA_percent_RS422M(0) = 1.0;
        dutyB_percent_RS422M(0) = 1.0;
    }
#endif
    //TEST4.1 (単純な8bit,RS422)
#if 0
    uint8_t a = 0;
    device_id[0].data_id[2].status.type = recieve_DATAtype;
    device_id[0].data_id[2].length = 1;
    device_id[0].data_id[2].status.Once_BidirectionalCommunication = 0;
    device_id[0].data_id[1].status.type = transmit_DATAtype;
    device_id[0].data_id[1].length = 1;
    device_id[0].data_id[1].status.Once_BidirectionalCommunication = 0;
    setupUART();
    while (1) {
        device_id[0].data_id[1].data[0] = device_id[0].data_id[2].data[0];
    }
#endif
    //TEST5(KONDOサーボの駆動)
#if 0
    setupUART();
    uint16_t data_waste;
    uint16_t a = 5520, i = 0;

    while (1) {
        //_LATD8 = 1;
        SetPosition(0, degToPos(180), NULL, false);
        //_LATD8 = 0;
        for (i = 0; i < 100; i++) {
            __delay_ms(10);
        }

        SetPosition(0, degToPos(90), &data_waste, true);
        for (i = 0; i < 100; i++) {
            __delay_ms(10);
        }

    }
#endif
    //TEST5.10
#if 0
    setdelay(3, micro_mode_TMR, (((1.0 / UART4_bps) * 100.0) * 1000000.0), ON);
    while (1);
#endif
    //TEST6(シリンダの駆動)
#if 0
    setupUART();
    while (1) {
        Cylinder_SW();
    }
#endif
    //TEST7(RS422 PID 位置制御)
#if 0
    uint8_t str[32] = {}, i = 0;
    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    setupUART();
    init_state_Ctrl();
    reset_All_RS422();
    /* 変数の値だけ入れておく */
    setPIDcoefficients(6.0, 2.0, 1.0);
    setLiftSpeed(1.0);
    setLiftPosition(30.0);
    /* PWMの設定 */
    initLiftRawPWM();
    //init_Position_RS422(1);
    setLiftDuty(0.8);
    restart_RS422();
    safetyLock(true);
    initLiftFeedBack();
    uint8_t SW = 0;
    while (1) {
        if (STAP_mode_select(1) == POSITION_CONTROLLER) {
            clearScreen_Ctrl();
            sprintf(str, "%4.2f %4.2f %4.2f", now_position_A_RS422M(1), now_Dposition_A_RS422M(1), Dposition_A_RS422M(1));
            set_next_mes_FIFO(0, 0, str);
            sprintf(str, "%5.2f %1d %1d", now_duty_A_RS422M(1), STAP_mode_select(1), state_RS422.now_RS422);
            set_next_mes_FIFO(1, 0, str);
        } else if (STAP_mode_select(1) == PWM_CONTROLLER) {
            clearScreen_Ctrl();
            sprintf(str, "%4.2f %1d", dutyA_percent_RS422M(1), STAP_mode_select(1));
            set_next_mes_FIFO(0, 0, str);
            //setLiftDuty(0.8);
        }


        if (right_cross_Right) {
            //initLiftFeedBack();
        } else if (right_cross_Left) {
            //initLiftRawPWM();
        }
        Nx10_ms(3);


    }
#endif
    //TEST8(メカナムの駆動)
#if 0
    setupUART();

    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    while (1);
#endif
    //TEST9(手動操作)
#if 0
    reset_All_RS422();
    init_PWM_RS422(0);
    init_state_Ctrl();
    restart_RS422();
    setupUART();
    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    while (1) {
        Cylinder_SW();
    }
#endif
    //TEST10(自動操作追加)
#if 1

    reset_All_RS422();
    init_PWM_RS422(0);
    init_state_Ctrl();
    setupUART();
    safetyLock(1);
    constConfig(param, PARAM_NUM);
    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    safetyLock(0);
    restart_RS422();
    setPIDcoefficients(Kp, Ki, Kd);
    setLiftSpeed(liftSpeed);
    initLiftRawPWM();
    SetPosition(1, degToPos(kondoPositions[1] + kondoOffset[0]), NULL, false);
    SetPosition(2, degToPos(kondoPositions[1] + kondoOffset[1]), NULL, false);
    while (1) {
        if (ConfigStartSW) {
            safetyLock(1);
            constConfig(param, PARAM_NUM);
            safetyLock(0);
        }
#if 1
#endif
    }
#endif
    //TEST10.1(リミットフィルタ)
#if 0
    int i;
    while (1) {
        __delay_ms(10);
        sampling_filter_limit();
    }
#endif
    //TEST11(コントローラデバッグ)
#if 0
    uint8_t str[32] = {1, 2, 3, 4, 0x10, 0x20, 0x30, 0x40, 0xAA, 0xAB, 0xAC, 0xAF};

    init_state_Ctrl();
    setupUART();
    while (1) {
        //putUART3(0xAA);
        set_next_mes_FIFO(0, 0, "SW_is_truning_ON\r\n");
        Nx10_ms(2);
    }
#endif


    /* main PROGRAM*/
#if 0
    /*各モジュールのセットアップ*/
    setdelay(1, milli_mode_TMR, 10.0, ISR_ON);
    //setupBluetooth1_slave();
    setupUART();
    /*RS422 設定　*/
    Set_State_RS422();

    while (1) {

    }
#endif
    return 0;
}
/*
 End of File
 */
