/* 
 * File:   function_2016_japancap.h
 * Author: Shuuya
 *
 * Created on October 25, 2016, 11:26 AM
 */

#ifndef FUNCTION_2016_JAPANCAP_H
#define	FUNCTION_2016_JAPANCAP_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <float.h>
#include <math.h>

#include "RS422M.h"
#include "PIC24FJxxxGC006_head.h"
#include "KONDO.h"
#include "LimitSwitch.h"

#include "controller.h"
#if 0//前回の配置
#define FrontUpSW (Tact_SW_LeftUp && Tact_SW_RightDown)
#define FrontDownSW (Tact_SW_LeftDown && Tact_SW_RightDown)
#define BackUpSW (Tact_SW_MiddleUp && Tact_SW_RightDown)
#define BackDownSW (Tact_SW_MiddleDown && Tact_SW_RightDown)
#define AllUpSW (right_cross_Up && Tact_SW_RightDown)
#define AllDownSW (right_cross_Down && Tact_SW_RightDown)
#define ArmCloseSW (right_cross_Left)
#define ArmOpenSW (right_cross_Right)
#define KickSW (Tact_SW_RightUp && Tact_SW_RightDown)
#define SequenceFunctionSW Tact_SW_RightDown
#define SequenceGoSW left_cross_Right
#define SequenceBackSW left_cross_Left
#define SequenceIndex1SW left_cross_Up
#define SequenceIndex2SW left_cross_Down
#define SequenceYesSW (middle_SW_RightDown&&!middle_SW_LeftDown)
#define UnlockSW (!middle_SW_RightDown&&middle_SW_LeftDown)
#define ConfirmEnableSW mode_SW_RightRight
#define SequenceEnableSW mode_SW_LeftRight

#define slowButton left_lever_Right
#define accelButton right_lever_Left

#define ConfigNextSW left_cross_Right
#define ConfigBackSW left_cross_Left
#define ConfigUpSW left_cross_Up
#define ConfigDownSW left_cross_Down
#define ConfigEnableSW debug_run_SW_GetValue()
#define ConfigEndSW Tact_SW_RightDown
#define ConfigStartSW (middle_SW_RightDown&&middle_SW_LeftDown)

#define LiftUpSW (middle_SW_RightUp && (!Tact_SW_RightDown))
#define LiftDownSW (middle_SW_RightMiddle && (!Tact_SW_RightDown))
#define LiftManualUpSW (middle_SW_RightUp && (Tact_SW_RightDown))
#define LiftManualDownSW (middle_SW_RightMiddle && (Tact_SW_RightDown))

#define KondoUpSW (middle_SW_LeftUp && (!Tact_SW_RightDown))
#define KondoDownSW (middle_SW_LeftMiddle && (!Tact_SW_RightDown))
#else
#define FrontUpSW (left_lever_Right && !mode_SW_LeftRight)
#define FrontDownSW (left_lever_Left && !mode_SW_LeftRight)
#define BackUpSW (right_lever_Left && !mode_SW_LeftRight)
#define BackDownSW (right_lever_Right && !mode_SW_LeftRight)
#define AllUpSW (left_cross_Up && Tact_SW_LeftUp)
#define AllDownSW (left_cross_Down && Tact_SW_LeftUp)
#define ArmCloseSW (right_cross_Right)
#define ArmOpenSW (right_cross_Left)
#define KickSW (Tact_SW_RightDown)
#define SequenceFunctionSW middle_SW_RightDown
#define SequenceGoSW middle_SW_RightUp
#define SequenceBackSW middle_SW_RightMiddle
#define SequenceIndex1SW Tact_SW_MiddleUp
#define SequenceIndex2SW Tact_SW_MiddleDown
#define SequenceYesSW (Tact_SW_RightUp)
#define UnlockSW (Tact_SW_LeftDown)
#define ConfirmEnableSW mode_SW_RightRight
#define SequenceEnableSW mode_SW_LeftRight

#define slowButton left_cross_Left
#define accelButton left_cross_Right

#define ConfigNextSW left_cross_Right
#define ConfigBackSW left_cross_Left
#define ConfigUpSW left_cross_Up
#define ConfigDownSW left_cross_Down
#define ConfigEnableSW debug_run_SW_GetValue()
#define ConfigEndSW Tact_SW_RightDown
#define ConfigStartSW (Tact_SW_MiddleUp&&Tact_SW_MiddleDown)

#define LiftUpSW (middle_SW_LeftUp && (!Tact_SW_LeftUp))
#define LiftDownSW (middle_SW_LeftMiddle && (!Tact_SW_LeftUp))
#define LiftSetSW (middle_SW_LeftDown && (!Tact_SW_LeftUp))
#define LiftManualUpSW (middle_SW_LeftUp && (Tact_SW_LeftUp))
#define LiftManualDownSW (middle_SW_LeftMiddle && (Tact_SW_LeftUp))

#define KondoUpSW (right_cross_Up)
#define KondoDownSW (right_cross_Down)
#endif

    typedef enum sqen {
        SS_Init,
        SS_Start,
        SS_FrontLegUp,
        SS_BackLegUp,
        SS_KickFromHarbor,
        SS_ArrivalAtIsland,
        SS_ArrivalAtIsland2,
        SS_KickFromIsland,
        SS_ArrivalAtNewFrontier,
        SS_FrontLegDownPrev1,
        SS_FrontLegDownPrev2,
        SS_FrontLegDown,
        SS_BackLegDownPrev1,
        SS_BackLegDownPrev2,
        SS_BackLegDownPrev3,
        SS_BackLegDown,
        SS_End,
    } SequenceState;

    typedef enum whst {
        wheel_gradual,
        wheel_gradual_lock,
        wheel_prompt,
    } wheelState;

    typedef enum whspd {
        wheel_fast,
        wheel_normal,
        wheel_slow,
    } wheelSpeed;

    typedef enum lftseq {
        lift_init,
        lift_searchOrigin1,
        lift_searchOrigin2,
        lift_normal,
    } liftState;

    typedef union {
        uint8_t byte;

        struct {
            unsigned CNT1 : 4;
            unsigned CNT2 : 4;
        };

    } CNT_4bit;

    extern bool useful_controller;

    /*
    void init_mecanum_datas(void); //各種データのEEPROMからの読み込み
    void mecanum_control_2016(void); //各種メカナム関連ボタンの処理
    void mecanum_TMR_ISR(void); //メカナムの駆動の操作性向上関数(TMR1にて呼び出し 10ms間隔)
    void mecanum_TMR_ISR_Auto(void); //自動化も追加した関数
    void motor_Enable();//メカナム・中輪操作の有効化
    void motor_Disable();//メカナム・中輪操作の無効化
     */
    void Cylinder_SW(void); //各シリンダ用SW

    void put_char_Controller(void);


    /* 各割り込み関数 */
    void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void); //MON
    void __attribute__((interrupt, auto_psv)) _U1TXInterrupt(void); //MON

    void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void); //CONTROLLER
    void __attribute__((interrupt, auto_psv)) _U2TXInterrupt(void); //CONTROLLER

    void __attribute__((interrupt, auto_psv)) _U3RXInterrupt(void); //USB
    void __attribute__((interrupt, auto_psv)) _U3TXInterrupt(void); //USB

    void __attribute__((interrupt, auto_psv)) _U4RXInterrupt(void); //KONDO
    void __attribute__((interrupt, auto_psv)) _U4TXInterrupt(void); //(NOT USED)KONDO

    void __attribute__((interrupt, auto_psv)) _T1Interrupt(void); //mecanum CONTROLLER_Timeout
    void __attribute__((interrupt, auto_psv)) _T3Interrupt(void); //KONDO
    void __attribute__((interrupt, auto_psv)) _T4Interrupt(void); //???
    void __attribute__((interrupt, auto_psv)) _T5Interrupt(void); //MON
    void __attribute__((interrupt, auto_psv)) _MI2C1Interrupt(void); //EEPROM


    /* KONDO.h */
    /*定義しろ*/
    void KondoPutByte(uint8_t data); //1バイト送信関数
    uint8_t KondoGetByte(); //1バイト受信関数
    bool KondoGettable(); //有効なデータがあるかどうか
    void KondoSetRXIE(bool en); //受信割り込み許可設定
    void KondoClearRXIF(); //受信割り込みフラグクリア
    void KondoStartTimer(); //タイムアウトタイマー初期化&スタート , (1/n_bps)*100
    void KondoStopTimer(); //タイムアウトタイマー停止


    void DEBUG_puts(const char* string);

    typedef union limituni {

        struct {
            unsigned Landing : 1;
            unsigned FrontUp : 1;
            unsigned FrontDown : 1;
            unsigned BackUp : 1;
            unsigned BackDown : 1;
            unsigned Kick : 1;
        };
        uint16_t byte;
    } limit;
    limit sampling_filter_limit();
    /**************************************************************************/
    /* <コントローラ送信> */
    void putUART2_2016(uint8_t);
    void stop_TX_UART2_2016();
    void start_TX_UART2_2016();
    /*<デバッグ送信>*/
    void putUART3_2016(uint8_t);
    void stop_TX_UART3_2016();


    /* KONDO.h */
    /*定義しろ*/
    void KondoPutByte(uint8_t data); //1バイト送信関数
    uint8_t KondoGetByte(); //1バイト受信関数
    bool KondoGettable(); //有効なデータがあるかどうか
    void KondoSetRXIE(bool en); //受信割り込み許可設定
    void KondoClearRXIF(); //受信割り込みフラグクリア
    void KondoStartTimer(); //タイムアウトタイマー初期化&スタート , (1/n_bps)*100
    void KondoStopTimer(); //タイムアウトタイマー停止
    /****************************/
    void safetyLock(bool en);
    /************昇降**********/
    void initLiftRawPWM();

    void setLiftDuty(float duty);
    float getNowPosition();
    void initLiftFeedBack();

    void setLiftSpeed(float spd);

    void setLiftPosition(float pos);

    void setPIDcoefficients(float Kp, float Ki, float Kd);

#define LIFT_POS_NUM 5
#define KONDO_POS_NUM 3
#ifdef	__cplusplus
}
#endif

#endif	/* FUNCTION_2016_JAPANCAP_H */

