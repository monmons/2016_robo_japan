#include "controller.h"

#define RingBuffer_SIZE 100
#define MAX_RingBuffer_NUM (RingBuffer_SIZE - 1)
uint8_t FIFO_ASCii[RingBuffer_SIZE] = {};
static uint8_t output_position = 0; //送りだす、添え字
static uint8_t input_position = 0; //代入すべき、添え字
state_Ctrl now_state_Ctrl = {};

UART_1byte_TX putchar_Controller = putUART2_2016; //送信する関数
UART_ST stop_TX_Controller = stop_TX_UART2_2016; //送信割り込みの停止
UART_ST start_TX_Controller = start_TX_UART2_2016; //送信割り込みのqsdwefrgt

#define now_send_mes_Ctrl (FIFO_ASCii[output_position])
#define buffer_is_empty (input_position == output_position)

void init_state_Ctrl(void) {

    set_data_none_Ctrl();
    set_space_exist_Ctrl();

}

bool putmes(uint8_t c) {

    stop_TX_Controller();
    if (now_space_exist_Ctrl) {
        FIFO_ASCii[input_position++] = c;
        if (input_position >= RingBuffer_SIZE) {
            input_position = 0;
        }
        set_data_exist_Ctrl();
        if (input_position == output_position) {
            set_space_none_Ctrl();
        }
        start_TX_Controller();
        return true;
    } else {
        start_TX_Controller();
        return false;
    }
}

void FIFO_send_Controller_ISR(void) {

    /* <実際にUARTで送り出すところ> */
    if (now_data_exist_Ctrl) {//バッファにデータがある
        putchar_Controller(now_send_mes_Ctrl); //1byteの転送
        set_space_exist_Ctrl();
        output_position++;
        if (output_position >= RingBuffer_SIZE) {
            output_position = 0;
        }
        if (output_position == input_position) {
            set_data_none_Ctrl(); //データがない
            stop_TX_Controller(); //送信割り込みの停止
        }
    }
}

void set_next_mes_FIFO(uint8_t li, uint8_t col, const uint8_t* datas) {
    //送りたい文字列を代入,バッファに格納
    uint8_t i = 0;

    /*************************************/
    /* <縦、横> */
    putmes('\x11');
    putmes(li);
    putmes('\x12');
    putmes(col);
    /*************************************/
    while (datas[i] != '\0') {
        putmes(datas[i++]);
        /*バッファに空きがないとき*///工事中
    }
}

void clearScreen_Ctrl() {
    putmes('\0');
    putmes('\0');
}
