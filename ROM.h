/* 
 * File:   ROM.h
 * Author: medama1221
 *
 * Created on 2015/07/11, 14:53
 */

#ifndef ROM_H
#define	ROM_H
#include <stdint.h>
#include "mcc_generated_files/mcc.h"

#ifdef	__cplusplus
extern "C" {
#endif
#define ROM_SIZE (64UL*1024UL/8)//0x2000
#define PAGE_SIZE 32//24LC64
#define ROM_ADDR     (0x50|0x00)//A2A1A0=0b000
#if (ROM_SIZE>256)
#define ADD_SIZE 2
#else
#define ADD_SIZE 1
#endif
    bool romWrite(uint8_t romadd,uint16_t add,uint8_t* data,uint8_t length);
    bool romRead(uint8_t romadd,uint16_t add,uint8_t* data,uint8_t length);
    bool isReady(uint8_t romadd);


#ifdef	__cplusplus
}
#endif

#endif	/* ROM_H */

