/*
 * File:   constConfig.c
 * Author: Robo6
 *
 * Created on October 27, 2016, 3:47 PM
 */
#include "constConfig.h"
#include "ROM.h"
#include <stdio.h>
#include <math.h>
#define LOOP_RATE 10//ループ時間

#define FRAME_RATE 50//表示更新時間
#define CLICK_JUDGE_TIME 50//押されたと判断するための時間[ms]
#define LONGPRESS_JUDGE_TIME 1000//長押しされたと判断するための時間[ms]
#define RAPIDFIRE_JUDGE_TIME 100//長押ししている間は何[ms]ごとにパラメータを上げるか？

typedef enum butststrc {
    waitClicked,
    waitLongPressed,
    waitRapidFire,
    invalid,
} ButtonState;

uint16_t waitTime[] = {
    CLICK_JUDGE_TIME / LOOP_RATE,
    LONGPRESS_JUDGE_TIME / LOOP_RATE,
    RAPIDFIRE_JUDGE_TIME / LOOP_RATE,
};
#define STR_LENGTH 30

void constConfig(Parameter* param, uint16_t n) {
    uint16_t index = 0;
    uint16_t cnt = 0;
    ConfigButton rowButton; //生のボタンデータ
    ConfigButton beforeButton; //前のボタンデータ
    ConfigButton clickedButton; //どれが押されている？
    ButtonState waitState = waitClicked;
    Parameter* paramPtr = param;
    uint16_t pushCounter = 0;
    uint16_t i;
    char str[STR_LENGTH];
    rowButton.word = beforeButton.word = 0x0000;
    clickedButton.word = 0x0000;
    wait_ms(100);
    rowButton = getButtons();
    if (rowButton.end) {
        return;
    }
    clearDisp();
    putsDisp(0, 0, "HYDRA");
    putsDisp(1, 1, "Configurator");
    wait_ms(100);
    putsDisp(1, 1, "M");
    wait_ms(100);
    putsDisp(1, 1, "C");
    wait_ms(100);
    for (i = 0, paramPtr = param; i < n; i++, paramPtr++) {
        romRead(ROM_ADDR, i * sizeof (float), (uint8_t*) paramPtr->address, sizeof (float));
    }
    paramPtr = param;
    while (1) {
        rowButton = getButtons();
        if (rowButton.end) {
            break;
        }
        clickedButton.word = 0x0000;
        if ((rowButton.word != 0) && (rowButton.word == beforeButton.word)) {
            if ((++pushCounter) >= waitTime[waitState]) {
                pushCounter = 0;
                if (waitState != waitRapidFire) {
                    waitState++;
                }
                clickedButton.word = rowButton.word;
            }
        } else {
            pushCounter = 0;
            waitState = waitClicked;
        }
        beforeButton = rowButton;

#if 1
        switch (clickedButton.word) {
            case CONF_BTN_NEXT:
                if (index + 1 < n) {
                    paramPtr++;
                    index++;
                }
                break;
            case CONF_BTN_BACK:
                if (index > 0) {
                    paramPtr--;
                    index--;
                }
                break;
            case CONF_BTN_UP:
                *(paramPtr->address) += paramPtr->upRate;
                if (!(*(paramPtr->address) <= paramPtr->max)) {
                    *(paramPtr->address) = paramPtr->max;
                }
                if ((-1e-3 < (*(paramPtr->address)))&&(*(paramPtr->address) < 1e-3)) {
                    (*(paramPtr->address)) = 0.0;
                }
                break;
            case CONF_BTN_DOWN:
                *(paramPtr->address) -= paramPtr->upRate;
                if (!(*(paramPtr->address) >= paramPtr->min)) {
                    *(paramPtr->address) = paramPtr->min;
                }
                if ((-1e-3 < (*(paramPtr->address)))&&(*(paramPtr->address) < 1e-3)) {
                    (*(paramPtr->address)) = 0.0;
                }


                break;
            default:
                break;
        }
#endif
        if ((++cnt) >= FRAME_RATE / LOOP_RATE) {
            cnt = 0;
            clearDisp();
            sprintf(str, "%02x:%13.13s", index, (paramPtr->name));
            putsDisp(0, 0, str);
            sprintf(str, "%16.2f", (double) *(paramPtr->address));
            putsDisp(1, 0, str);
        }

        wait_ms(LOOP_RATE);
    }
    for (i = 0, paramPtr = param; i < n; i++, paramPtr++) {
        romWrite(ROM_ADDR, i * sizeof (float), (uint8_t*) paramPtr->address, sizeof (float));
                }
    clearDisp();
    putsDisp(0, 0, "Saving");
    for (i = 0; i < 10; i++) {
        wait_ms(50);
        putsDisp(0, i + 6, ".");
    }
    wait_ms(200);
}