#ifndef FCY
#define FCY (16000000.0)
#endif

#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>
#include "PIC24FJxxxGC006_head.h"
#include "mcc_generated_files/mcc.h"

uint16_t T2CKPS = 0;
uint16_t T3CKPS = 0;
uint8_t Controller_variables[8] = {};


void setupUART(void) {

    /*UART4 : KONDO用 とする*/

    _U4RXIF = 0;
    _U4RXIP = UART4RX_IP;
    _U4RXIE = 1;

    _U4TXIE = 0;
    _U4TXIP = UART4TX_IP;
    _U4TXIF = 0;

    /*UART3 : PC_to_EEPROM とする*/

    _U3RXIF = 0;
    _U3RXIP = UART3RX_IP;
    _U3RXIE = 1;

    _U3TXIF = 0;
    _U3TXIP = UART3TX_IP;
    _U3TXIE = 0;

    /*UART2 : コントローラ とする*/
    _U2RXIF = 0;
    _U2RXIP = UART2RX_IP;
    _U2RXIE = 1;

    _U2TXIF = 1;
    _U2TXIP = UART2TX_IP;
    _U2TXIE = 0;

    /*UART1 : RS422 とする*/

    _U1RXIF = 0;
    _U1RXIP = UART1RX_IP;
    _U1RXIE = 1;

    _U1TXIF = 1;
    _U1TXIP = UART1TX_IP;
    _U1TXIE = 0;
    return;
}

unsigned char makeASCII(unsigned char result) {

    if (result <= 9) {
        result = result + 48;
    } else {
        result = result + 55;
    }

    return result;

}

void makeASCIIs(uint8_t mode, int16_t result, uint8_t* returns) {//mode,result,returns

    if (result > 0) {
        returns[0] = 43;
    } else {
        returns[0] = 45;
    }

    result = abs(result);

    if (mode == 16) {
        returns[4] = result & 0x000F;
        returns[4] = makeASCII(returns[4]);

        returns[3] = (result >> 4)& 0x000F;
        returns[3] = makeASCII(returns[3]);

        returns[2] = (result >> 8)& 0x000F;
        returns[2] = makeASCII(returns[2]);

        returns[1] = (result >> 12)& 0x000F;
        returns[1] = makeASCII(returns[1]);

        returns[5] = 32; //スペース

    } else if (mode == 10) {
        returns[1] = (result / 10000);
        returns[2] = (result / 1000)-(returns[1]*10);
        returns[3] = (result / 100)-(returns[2]*10 + returns[1]*100);
        returns[4] = (result / 10)-(returns[3]*10 + returns[2]*100 + returns[1]*1000);
        returns[5] = result - (returns[4]*10 + returns[3]*100 + returns[2]*1000 + returns[1]*10000);

        returns[1] = makeASCII(returns[1]);
        returns[2] = makeASCII(returns[2]);
        returns[3] = makeASCII(returns[3]);
        returns[4] = makeASCII(returns[4]);
        returns[5] = makeASCII(returns[5]);

    }

    returns[6] = 32; //スペース

    return;
}

bool check_data_Controler(uint8_t* data, uint8_t data_num) {
    //data 確認するデータ
    //data_num　現在データ番号

    /*  S   */
    if (data_num == 0) {
        return (data[data_num] == 'S');
    }
    /*　E   */
    if (data_num == 18) {
        if (data[data_num] == 'E') {
            return true;
        } else {
            return false;
        }
    }
    /*  2回目 SもしくはE*/
    if ((data_num == 1) || (data_num == 19)) {
        return (data[data_num] == data[data_num - 1]); //２回目のSもしくはEのとき
    }
    /*  実データ部  */
    if (data_num % 2 == 0) {
        return true; //偶数回目の受信
    } else {
        return (data[data_num] == (data[data_num - 1] ^ 0xFF));
    }//奇数回目で偶数回目の反転

    return false;
}

bool Controler_checker_ISR(uint8_t using_UART) {

    static Controler_status status = {};
    static uint8_t direct_data[20] = {};
    uint8_t i;
    uint8_t* add_uart_state = &(((uint8_t *) &U1STA)[0]);
    
    if (U2STAbits.OERR || U2STAbits.FERR) {
        U2MODEbits.UARTEN=0;
        U2MODEbits.UARTEN=1;
    }
    /**************************************************************************/
    uint8_t k = 1;
    uint8_t h = 0;

    for (i = 1; i <= using_UART; i++) {
        k = k*i;
        h += k;
    }
    h = h * 0x10;
    add_uart_state = add_uart_state + h;
    /**************************************************************************/
    //while ((((uint16_t*) add_uart_state)[0] & 0x1) != 0) {//_RXDA==1
    while (U2STAbits.URXDA != 0) {
        if (using_UART == 1) {
            direct_data[status.recievetime] = U1RXREG;
            _U1RXIF = 0;
        } else if (using_UART == 2) {
            direct_data[status.recievetime] = U2RXREG;
            _U2RXIF = 0;
        } else if (using_UART == 3) {
            direct_data[status.recievetime] = U3RXREG;
            _U3RXIF = 0;
        } else if (using_UART == 4) {
            direct_data[status.recievetime] = U4RXREG;
            _U4RXIF = 0;
        }
        if (check_data_Controler(direct_data, status.recievetime) == true) {
            if (status.recievetime >= 19) {
                status.recievetime = 0;
                for (i = 0; i < 8; i++) {
                    Controller_variables[i] = direct_data[(i * 2) + 2];
                }
            } else {
                status.recievetime++;
            }
            status.useful = true;
        } else {
            status.useful = false;
            status.recievetime = 0;
        }
    }
    return status.useful;
}

void init_controller_data(void) {

    uint8_t i = 0;

    for (i = 0; i < 8; i++) {
        Controller_variables[i] = 0;
    }
    Controller_variables[2] = 127;
    Controller_variables[3] = 127;
    Controller_variables[4] = 127;
    Controller_variables[5] = 127;

    /*BT_reset_SetLow();
    Nx100_ms(1);
    BT_reset_SetHigh();
    */
    
}
#if 0
void setupTMR3(void) {

    T3CON = 0;
    TMR3 = 0;
    PR3 = 0xFFFF;

}
#endif
void setdelay(unsigned char move_nomber_of_TMR, unsigned char mode, float Sec, bool ISR_SW) {

    uint32_t TMR = 0.0;
    if (mode == milli_mode_TMR) {
        TMR = ((Sec * milli * FCY) / (256.0));
    } else if (mode == micro_mode_TMR) {
        TMR = ((Sec * micro * FCY) / (8.0));
    } else if (mode == nano_mode_TMR) {
        TMR = (Sec * nano * FCY);
    } else {
        TMR = 1;
    }
    /**************************************************************************/

    if (TMR > 0xFFFF) {
        TMR = 0xFFFF;
    }

    /**************************************************************************/

    if (move_nomber_of_TMR == 1) {
        T1CON = 0;
        if (mode == milli_mode_TMR) {
            T1CONbits.TCKPS = 3;
        } else if (mode == micro_mode_TMR) {
            T1CONbits.TCKPS = 1;
        } else if (mode == nano_mode_TMR) {
            T1CONbits.TCKPS = 0;
        }
        TMR1 = 0;
        PR1 = TMR;
        _T1IF = 0;
        if (ISR_SW == ON) {
            _T1IP = TMR1_IP;
            _T1IE = 1;
        } else {
            _T1IP = 0;
            _T1IE = 0;
        }
        T1CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 2) {
        T2CON = 0;
        if (mode == milli_mode_TMR) {
            T2CONbits.TCKPS = 3;
        } else if (mode == micro_mode_TMR) {
            T2CONbits.TCKPS = 1;
        } else if (mode == nano_mode_TMR) {
            T2CONbits.TCKPS = 0;
        }
        TMR2 = 0;
        PR2 = TMR;
        _T2IF = 0;
        if (ISR_SW == ON) {
            _T2IP = TMR2_IP;
            _T2IE = 1;
        } else {
            _T2IP = 0;
            _T2IE = 0;
        }
        T2CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 3) {
        T3CON = 0;
        if (mode == milli_mode_TMR) {
            T3CONbits.TCKPS = 3;
        } else if (mode == micro_mode_TMR) {
            T3CONbits.TCKPS = 1;
        } else if (mode == nano_mode_TMR) {
            T3CONbits.TCKPS = 0;
        }
        TMR3 = 0;
        PR3 = TMR;
        _T3IF = 0;
        if (ISR_SW == ON) {
            _T3IP = TMR3_IP;
            _T3IE = 1;
            check_LED_RS422_SetHigh();
        } else {
            _T3IP = 0;
            _T3IE = 0;
        }
        T3CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 4) {
        T4CON = 0;
        if (mode == milli_mode_TMR) {
            T4CONbits.TCKPS = 3;
        } else if (mode == micro_mode_TMR) {
            T4CONbits.TCKPS = 1;
        } else if (mode == nano_mode_TMR) {
            T4CONbits.TCKPS = 0;
        }
        TMR4 = 0;
        PR4 = TMR;
        _T4IF = 0;
        if (ISR_SW == ON) {
            _T4IP = TMR4_IP;
            _T4IE = 1;
        } else {
            _T4IP = 0;
            _T4IE = 0;
        }
        T4CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 5) {
        T5CON = 0;
        if (mode == milli_mode_TMR) {
            T5CONbits.TCKPS = 3;
        } else if (mode == micro_mode_TMR) {
            T5CONbits.TCKPS = 1;
        } else if (mode == nano_mode_TMR) {
            T5CONbits.TCKPS = 0;
        }
        TMR5 = 0;
        PR5 = TMR;
        _T5IF = 0;
        if (ISR_SW == ON) {
            _T5IP = TMR5_IP;
            _T5IE = 1;
        } else {
            _T5IP = 0;
            _T5IE = 0;
        }
        T5CONbits.TON = 1;

        return;
    }
    return;
}

#if 0
void setupOC(void) {

    if (OC_1and2_period > 0) {
        OC1R = 0;
        OC1RS = 0;
        OC1CON1 = 0;
        OC2R = 0;
        OC2RS = 0;
        OC2CON1 = 0;
        T2CON = 0;
        PR2 = 0;

        if (OC_1and2_period < 1600.0) {
            T2CKPS = 1;
            OC1RS = (((OC_1and2_period) * (Fcyc / 2.0)) - 1.0);
            T2CONbits.TCKPS = 0;
        } else if (OC_1and2_period < 13000.0) {
            T2CKPS = 8;
            OC1RS = (((OC_1and2_period) * (Fcyc / 2.0)) - 1.0);
            T2CONbits.TCKPS = 1;
        } else if (OC_1and2_period < 100000.0) {
            T2CKPS = 64;
            OC1RS = (((OC_1and2_period) * (Fcyc / 2.0)) - 1.0);
            T2CONbits.TCKPS = 2;
        } else if (OC_1and2_period < 838848.0) {
            T2CKPS = 256;
            OC1RS = (((OC_1and2_period) * (Fcyc / 2.0)) - 1.0);
            T2CONbits.TCKPS = 3;
        } else {

            T2CKPS = 256;
            OC1RS = 0xFFFF;
            T2CONbits.TCKPS = 3;
        }

        PMD2bits.OC1MD = 0;
        OC1CON1bits.OCTSEL = 0; //TMR2
        OC1CON2bits.SYNCSEL = 0x1F;
        OC1CON1bits.OCM = 6;
        T2CONbits.TON = 1;

    }

    return;
}
#endif
void setduty_OC(uint8_t Move_Number_of_OC, bool pinfunction, float duty_percent) {

    //duty_percent は -1.0 ~ +1.0
    if (Move_Number_of_OC == 1) {
        if (pinfunction == true) {
            OC1R = ((uint16_t) ((OC1RS) * (fabs(duty_percent))));
        } else {
            OC1R = ((uint16_t) ((OC1RS) * (1.0 - fabs(duty_percent))));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == true) {
            OC2R = ((uint16_t) ((OC2RS) * (fabs(duty_percent))));
        } else {
            OC2R = ((uint16_t) ((OC2RS) * (1.0 - fabs(duty_percent))));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == true) {
            OC3R = ((uint16_t) ((OC3RS) * (fabs(duty_percent))));
        } else {
            OC3R = ((uint16_t) ((OC3RS) * (1.0 - fabs(duty_percent))));
        }
    } else {
        if (pinfunction == true) {
            OC4R = ((uint16_t) ((OC4RS) * (fabs(duty_percent))));
        } else {
            OC4R = ((uint16_t) ((OC4RS) * (1.0 - fabs(duty_percent))));
        }
    }

    return;

}

#if 0
void setdutySec_for_OC(unsigned char Move_Number_of_OC, bool pinfunction, unsigned short MicroSec) {

    if (Move_Number_of_OC == 1) {
        if (pinfunction == true) {
            OC1RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC1RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == true) {
            OC2RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC2RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == true) {
            OC3RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {
            OC3RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    } else {
        if (pinfunction == true) {
            OC4RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {

            OC4RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    }

    return;
}
#endif
void setMotor_State(uint8_t num_Motor, float duty_percent) {

    if (num_Motor == 1) {

        setduty_OC(1, active_LOW, duty_percent);

        if ((duty_percent) == 0.0) {
            SET_BRAKE_A(); //ブレーキ
        } else if ((duty_percent) > 0.0) {
            SET_TRUN_NORMAL_A(); //正転
        } else {
            SET_TRUN_REVERSE_A(); //逆転
        }

    } else if (num_Motor == 2) {

        setduty_OC(2, active_LOW, duty_percent);

        if ((duty_percent) == 0.0) {
            SET_BRAKE_B(); //ブレーキ
        } else if ((duty_percent) > 0.0) {
            SET_TRUN_NORMAL_B(); //正転
        } else {
            SET_TRUN_REVERSE_B(); //逆転
        }

    } else if (num_Motor == 3) {

        setduty_OC(3, active_LOW, duty_percent);

        if ((duty_percent) == 0.0) {
            SET_BRAKE_C(); //ブレーキ
        } else if ((duty_percent) > 0.0) {
            SET_TRUN_NORMAL_C(); //正転
        } else {

            SET_TRUN_REVERSE_C(); //逆転
        }

    } else if (num_Motor == 4) {

        setduty_OC(4, active_LOW, duty_percent);

        if ((duty_percent) == 0.0) {
            SET_BRAKE_D(); //ブレーキ
        } else if ((duty_percent) > 0.0) {
            SET_TRUN_NORMAL_D(); //正転
        } else {
            SET_TRUN_REVERSE_D(); //逆転
        }

    }

    return;
}

/*メカナム*/
float result[4] = {};

#if 0
float* mecanum_Control(bool GOorTURN, uint8_t x_mecanum, uint8_t y_mecanum) {

    float x_calculation = 0.0, y_calculation = 0.0; //符号付きの処理

    x_calculation = ((int8_t) (x_mecanum - 128));
    y_calculation = ((int8_t) (y_mecanum - 128)) * (-1.0);

    float duty_FR_percent = 0.0, duty_FL_percent = 0.0;
    float duty_BR_percent = 0.0, duty_BL_percent = 0.0;
    //F:flont(前) B:back(後)
    //R:right(右) L:left(左)
    //例:FR(前右)
    float middle_calculation = 0.0;

    /*  十字型　不感帯*/
    if (fabs(x_calculation) < min_go_area) {
        x_calculation = min_go_area;
    }
    if (fabs(y_calculation) < min_go_area) {
        y_calculation = min_go_area;
    }

    /******************     上限設定１        ******************************/
    /*************/
    /* x値の調整 */
    /*************/
    if (x_calculation > 0.0) {
        x_calculation = (x_calculation - min_go_area) * 1.0; //不感領域分を引く　*2.0は上がる傾き
        if (x_calculation >= 100.0) {
            x_calculation = 100.0;
        }//percentの上限設定
    }//正のとき
    else {
        x_calculation = (x_calculation + min_go_area) * 1.0; //不感領域分を引く　*2.0は上がる傾き
        if (x_calculation <= -100.0) {
            x_calculation = -100.0;
        }//percentの上限設定
    }//負のとき
    /************/
    /* y値の調整 */
    /************/
    if (y_calculation > 0.0) {
        y_calculation = (y_calculation - min_go_area) * 1.0; //不感領域分を引く　*2.0は上がる傾き
        if (y_calculation >= 100.0) {
            y_calculation = 100.0;
        }//percentの上限設定
    }//正のとき
    else {
        y_calculation = (y_calculation + min_go_area) * 1.0; //不感領域分を引く　*2.0は上がる傾き
        if (y_calculation <= -100.0) {
            y_calculation = -100.0;
        }//percentの上限設定
    }//負のとき
    /******************     途中計算　終了   ******************************/
    if (GOorTURN == GO_mode_mecanum) {

        duty_FR_percent = ((-1.0) * x_calculation) + y_calculation;
        duty_FL_percent = x_calculation + y_calculation;
        duty_BR_percent = duty_FL_percent; //BRはFLと同じ動き
        duty_BL_percent = duty_FR_percent; //BLはFRと同じ動き

    }//x方向とy方向の重ね合わせをしている

    else {
        if (x_mecanum > 0) {

            middle_calculation = (x_calculation - min_go_area);
            duty_FR_percent = middle_calculation;
            duty_FL_percent = (-1.0) * duty_FR_percent;
            duty_BR_percent = duty_FR_percent;
            duty_BL_percent = duty_FL_percent;

        } else {

            middle_calculation = (x_calculation + min_go_area);
            duty_FR_percent = middle_calculation;
            duty_FL_percent = (-1.0) * duty_FR_percent;
            duty_BR_percent = duty_FR_percent;
            duty_BL_percent = duty_FL_percent;

        }

    }//旋回

    /*      duty比の上限の設定      */
    if (duty_FR_percent > 100.0) {
        duty_FR_percent = 100.0;
    } else if (duty_FR_percent < -100.0) {
        duty_FR_percent = -100.0;
    }
    if (duty_FL_percent > 100.0) {
        duty_FL_percent = 100.0;
    } else if (duty_FL_percent < -100.0) {
        duty_FL_percent = -100.0;
    }
    if (duty_BR_percent > 100.0) {
        duty_BR_percent = 100.0;
    } else if (duty_BR_percent < -100.0) {
        duty_BR_percent = -100.0;
    }
    if (duty_BL_percent > 100.0) {
        duty_BL_percent = 100.0;
    } else if (duty_BL_percent < -100.0) {
        duty_BL_percent = -100.0;
    }
    /***************************************************/
    /*      duty比の設定        */
    /*dutyA_percent_RS422M(0) = (duty_FR_percent * 0.01);
    dutyB_percent_RS422M(0) = (duty_FL_percent * 0.01);
    dutyA_percent_RS422M(1) = (duty_BR_percent * 0.01);
    dutyB_percent_RS422M(1) = (duty_BL_percent * 0.01);*/

    result[0] = (duty_FR_percent);
    result[1] = (duty_FL_percent);
    result[2] = (duty_BR_percent);
    result[3] = (duty_BL_percent);

    /*setMotor_State(1 , (duty_FR_percent * 0.01));
    setMotor_State(2 , (duty_FL_percent * 0.01));
    setMotor_State(3 , (duty_BR_percent * 0.01));
    setMotor_State(4 , (duty_BL_percent * 0.01));*/


    return result;
}
#endif
void Nx100_ms(uint8_t times) {
    uint8_t times_Nx;
    uint8_t i;
    for (times_Nx = 0; times_Nx < times; times_Nx++) {
        for (i = 0; i < 10; i++) {
            __delay_ms(10);
        }

    }
    return;
}

void Nx10_ms(uint8_t times) {
    uint8_t times_Nx;
    for (times_Nx = 0; times_Nx < times; times_Nx++) {
        __delay_ms(10);
    }
    return;
}

