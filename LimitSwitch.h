/* 
 * File:   LimitSwitch.h
 * Author: Shuuya
 *
 * Created on 2016/09/11, 23:24
 */

#ifndef LIMITSWITCH_H
#define	LIMITSWITCH_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>
#include "mcc_generated_files/pin_manager.h"

#define limit_PORT1 limit_1_GetValue()
#define limit_PORT2 limit_2_GetValue()
#define limit_PORT3 limit_3_GetValue()
#define limit_PORT4 limit_4_GetValue()
#define limit_PORT5 limit_5_GetValue()
#define limit_PORT6 limit_6_GetValue()
#define limit_PORT7 limit_7_GetValue()
#define limit_PORT8 limit_8_GetValue()
#define limit_PORT9 limit_9_GetValue()
#define limit_PORT10 limit_10_GetValue()

#define SW_FR_UP limit_PORT1
#define SW_FL_UP limit_PORT2
#define SW_FR_and_FL_DOWN (!limit_PORT3)  
#define SW_BR_UP limit_PORT4
#define SW_BL_UP limit_PORT5
#define SW_BR_and_BL_DOWN (!limit_PORT6)  //
#define SW_kickR (!limit_PORT4)
#define SW_kickL (!limit_PORT5)
    
#define SW_FR_and_FL_UP (SW_FR_UP & SW_FL_UP)
#define SW_FR_or_FL_UP  (SW_FR_UP | SW_FL_UP)
    
#define SW_BR_and_BL_UP (SW_BR_UP & SW_BL_UP)  //
#define SW_kick_R_and_L   (SW_kickR & SW_kickL)  //蹴りだしのタイミングリミット
#define SW_LiftOrigin limit_PORT7//昇降機構の初期位置リミット

//void LimitSwitch_check();

#ifdef	__cplusplus
}
#endif

#endif	/* LIMITSWITCH_H */

