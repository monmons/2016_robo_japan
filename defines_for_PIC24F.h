/* 
 * File:   defines_for_PIC24F.h
 * Author: Shuuya
 *
 * Created on 2016/08/09, 19:03
 */

#ifndef DEFINES_FOR_PIC24F_H
#define	DEFINES_FOR_PIC24F_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>

#define StartUp_LED _LATG9//起動確認用LED

#define debug_LED _LATD7//デバッグ用LED
 
#define check_RS422_LED _LATE4
    
#define check_superRS422_LED _LATE3
    
#define check_controller_LED _LATE2
    
#define check_operationEEPROM_LED _LATE1
    
#define Fosc (8000000.0)//クロック周波数

#define Fcyc (32000000.0)
    
#ifndef FCY
#define FCY (Fcyc/2.0)
#endif

#define TCY (2.0 / Fcyc)//クロック周波数
    
    
#define GO_mode_mecanum false
    
#define TURN_mode_mecanum true
    
#define min_go_area (25)
#define max_go_area (115)
    
    /**/
#ifndef nano
#define nano  (1.0/1000000000.0)//単位系
#endif

#ifndef micro    
#define micro (1.0/1000000.0)//単位系
#endif
    
#ifndef milli
#define milli (1.0/1000.0)  //単位系
#endif
    
    
    
#ifndef nano_mode_TMR
#define nano_mode_TMR  0
#endif

#ifndef micro_mode_TMR    
#define micro_mode_TMR 1
#endif
    
#ifndef milli_mode_TMR
#define milli_mode_TMR 2
#endif
    
#ifndef	ISR_ON
#define ISR_ON 1
#endif
    
#ifndef	ISR_OFF
#define ISR_OFF 0
#endif    
    
#ifndef	ON
#define ON 1
#endif
    
#ifndef	OFF
#define OFF 0
#endif
    
#ifndef	active_HI
#define active_HI 1
#endif
    
#ifndef	active_LOW
#define active_LOW 0
#endif
    
#ifndef eight_bitsUARTtype
#define eight_bitsUARTtype 0
#endif

#ifndef nine_bitsUARTtype
#define nine_bitsUARTtype 1
#endif
    
#ifndef UART_data_type
#define UART_data_type 0
#endif  

#ifndef UART_add_type
#define UART_add_type 1
#endif    
    
#ifndef position_control
#define position_control 0
#endif

#ifndef speed_control
#define speed_control 1
#endif

    
    /*pin*/

#ifndef	not_used_module
#define not_used_module 200
#endif    

#ifndef	RP0
#define RP0 0
#endif
    
#ifndef	RP1
#define RP1 1
#endif
    
#ifndef	RP2
#define RP2 2
#endif
    
#ifndef	RP3
#define RP3 3
#endif
    
#ifndef	RP4
#define RP4 4
#endif
    
#ifndef	RP5
#define RP5 5
#endif
    
#ifndef	RP6
#define RP6 6
#endif
    
#ifndef	RP7
#define RP7 7
#endif
    
#ifndef	RP8
#define RP8 8
#endif
    
#ifndef	RP9
#define RP9 9
#endif
    
#ifndef	RP10
#define RP10 10
#endif
    
#ifndef	RP11
#define RP11 11
#endif
    
#ifndef	RP12
#define RP12 12
#endif
    
#ifndef	RP13
#define RP13 13
#endif
    
#ifndef	RP14
#define RP14 14
#endif
    
#ifndef	RP15
#define RP15 15
#endif
    
    /* PID用 */
#ifndef DPA_PID
#define DPA_PID (((double)(*(elements->P_A_gein))) * ((double)(ErrorA1-ErrorA2)))
#endif  
    
#ifndef DIA_PID
#define DIA_PID (((double)(*(elements->I_A_gein))) * ((double)(ErrorA1)))
#endif  

#ifndef DDA_PID
#define DDA_PID (((double)(*(elements->D_A_gein))) * ((double)((ErrorA1-ErrorA2)-(ErrorA2-ErrorA3))))
#endif  
    
#ifndef DPB_PID
#define DPB_PID (((double)(*(elements->P_B_gein))) * ((double)(ErrorB1-ErrorB2)))
#endif  
    
#ifndef DIB_PID
#define DIB_PID (((double)(*(elements->I_B_gein))) * ((double)(ErrorB1)))
#endif  

#ifndef DDB_PID
#define DDB_PID (((double)(*(elements->D_B_gein))) * ((double)((ErrorB1-ErrorB2)-(ErrorB2-ErrorB3))))
#endif  
    
    /*  Controller */
#define cross_SW Controller_variables[0]
#define left_cross_Down   (cross_SW & 0x8)
#define left_cross_Up     (cross_SW & 0x4)
#define left_cross_Right  (cross_SW & 0x2)
#define left_cross_Left   (cross_SW & 0x1)
#define right_cross_Down  (cross_SW & 0x80)
#define right_cross_Up    (cross_SW & 0x40)
#define right_cross_Right (cross_SW & 0x20)
#define right_cross_Left  (cross_SW & 0x10)    

    
#define middle_SW Controller_variables[1]
#define right_lever_Right     (middle_SW & 0x80)
#define right_lever_Left      (middle_SW & 0x40) 
#define left_lever_Left       (middle_SW & 0x10) 
#define left_lever_Right      (middle_SW & 0x20) 
#define middle_SW_RightMiddle (middle_SW & 0x8)
#define middle_SW_RightUp     (middle_SW & 0x4)
#define middle_SW_LeftMiddle  (middle_SW & 0x2)
#define middle_SW_LeftUp      (middle_SW & 0x1)
    
    
    
#define horizontal_lever_left       Controller_variables[2]//水平
#define perpendicular_lever_left    Controller_variables[3]//垂直
#define horizontal_lever_right      Controller_variables[4]//水平
#define perpendicular_lever_right   Controller_variables[5]//垂直

#define other_SW Controller_variables[6]
#define middle_SW_RightDown (other_SW & 0x80)
#define middle_SW_LeftDown  (other_SW & 0x40)
    
#define mode_SW_RightRight (other_SW & 0x02)
#define mode_SW_RightLeft (!(other_SW & 0x02))
    
#define mode_SW_LeftRight (other_SW & 0x01)
#define mode_SW_LeftLeft (!(other_SW & 0x01))
    
#define Tack_SW Controller_variables[7]
#define Tact_SW_RightDown   (Tack_SW & 0x20)
#define Tact_SW_MiddleDown   (Tack_SW & 0x10)
#define Tact_SW_LeftDown   (Tack_SW & 0x8)
#define Tact_SW_RightUp   (Tack_SW & 0x4)
#define Tact_SW_MiddleUp   (Tack_SW & 0x2)
#define Tact_SW_LeftUp   (Tack_SW & 0x1)



#ifdef	__cplusplus
}
#endif

#endif	/* DEFINES_FOR_PIC24F_H */

