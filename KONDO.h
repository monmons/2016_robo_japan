/* 
 * File:   KONDO.h
 * Author: medama1221
 *
 * Created on 2016/08/20, 16:54
 */

#ifndef KONDO_H
#define	KONDO_H
#include <stdint.h>
#include <stdbool.h>
#ifdef	__cplusplus
extern "C" {
#endif
    /*通信速度
     * 115200bps, 625000bps, 1.25Mbps
     * ビット長 8bit
     * スタート 1bit
     * ストップ 1bit
     * フロー制御 無し
     * パリティ EVEN（偶数）
     */
    
    /*定義しろ*/
    void KondoPutByte(uint8_t data);//1バイト送信関数
    uint8_t KondoGetByte();//1バイト受信関数
    bool KondoGettable();//有効なデータがあるかどうか
    void KondoSetRXIE(bool en);//受信割り込み許可設定
    void KondoClearRXIF();//受信割り込みフラグクリア
    void KondoStartTimer();//タイムアウトタイマー初期化&スタート
    void KondoStopTimer();//タイムアウトタイマー停止
#define KONDO_EEPROM_USE false

#define KONDO_BUF_SIZE 16

#define MAX_POS 11500
#define MIN_POS 3500
#define MOVE_RANGE_DEGREE 270.0f
#define degToPos(deg) ((uint16_t)((MAX_POS-MIN_POS)*((deg)/MOVE_RANGE_DEGREE)+MIN_POS))
#define posToDeg(pos) (pos-MIN_POS)/((float)(MAX_POS-MIN_POS))*(MOVE_RANGE_DEGREE)
    typedef enum {
        SET_POS = 0x80,
        GET_PRM = 0xa0,
        SET_PRM = 0xc0,
        SET_GET_ID = 0xe0,
    } CMD;

    typedef enum {
        EEP = 0x00,
        STR = 0x01,
        SPD = 0x02,
        CUR = 0x03,
        TMP = 0x04,
    } PRM_SC;

    typedef enum {
        GET = 0x00,
        SET = 0x01,
    } ID_SC;

    typedef enum {
        pending,
        successful,
        failed,
    } KONDO_COM_RESULT;
    
    /*
     *　id:サーボのID
     *  p:位置(3500-11500)(degToPosで角度(0-270)を位置にできるよ)
     *  val:現在位置(3500-11500)(posToDegで角度(0-270)にできるよ)を返すポインタ
     *  wait:trueのとき、またはvalがNULLじゃないときはコマンドが送信完了するまで関数からでない
     *      :falseでvalがNULLのときはコマンド送信完了する前に関数を抜ける(送信は割り込みを通して実行される)
     */
    bool SetPosition(uint8_t id, uint16_t p, uint16_t* val, bool wait);//位置指定
    bool ServoFree(uint8_t id, uint16_t* val, bool wait);//脱力
#if KONDO_EEPROM_USE
    void GetEEPROM(uint8_t id);
    void SetEEPROM(uint8_t id);
    bool GetID(bool wait);
    bool SetID(uint8_t id, uint8_t val, bool wait);
    
#endif
    bool GetParameter(uint8_t id, uint8_t sc, uint8_t* val, bool wait);//内部で使用
    bool SetParameter(uint8_t id, uint8_t sc, uint8_t val, bool wait);//内部で使用
#define GetStretch(id,val,stat) GetParameter(id,STR,val,stat)//ストレッチ取得
#define SetStretch(id,val,stat) SetParameter(id,STR,val,stat)//ストレッチ設定
#define GetSpeed(id,val,stat) GetParameter(id,SPD,val,stat)//スピード取得
#define SetSpeed(id,val,stat) SetParameter(id,SPD,val,stat)//スピード設定
#define GetCurrent(id,val,stat) GetParameter(id,CUR,val,stat)//電流取得
#define SetCurrent(id,val,stat) SetParameter(id,CUR,val,stat)//電流制限設定
#define GetTemperature(id,val,stat) GetParameter(id,TMP,val,stat)//温度取得
#define SetTemperature(id,val,stat) SetParameter(id,TMP,val,stat)//温度制限設定
    

    void KondoUARTRxTask();//受信割り込みでよんでくれ
    void KondoTimeOutTask();//タイマー割り込みで呼んでくれ
#ifdef	__cplusplus
}
#endif

#endif	/* KONDO_H */

