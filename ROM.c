#include "ROM.h"
#ifndef FCY
#define FCY 16e6
#endif
#include <libpic30.h>

#define SLAVE_I2C_GENERIC_RETRY_MAX           50
#define SLAVE_I2C_GENERIC_DEVICE_TIMEOUT      200   // define slave timeout 

bool romWrite(uint8_t romadd, uint16_t add, uint8_t* data, uint8_t length) {
    static I2C1_MESSAGE_STATUS status;
    static uint8_t writeBuffer[ADD_SIZE + PAGE_SIZE];
    static uint8_t *pD;
    static uint16_t counter, timeOut, slaveTimeOut;

    // build the write buffer first
    // starting address of the EEPROM memory
#if(ADD_SIZE==1)
    writeBuffer[0] = (uint8_t) (add); // low low address
#else
    writeBuffer[0] = (add >> 8); // high address
    writeBuffer[1] = (uint8_t) (add); // low low address
#endif

    pD = data;
    // data to be 
    for (counter = ADD_SIZE; counter < length + ADD_SIZE; counter++) {
        writeBuffer[counter] = *pD++;
    }
    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    timeOut = 0;
    slaveTimeOut = 0;
    status = I2C1_MESSAGE_PENDING;

    while (status != I2C1_MESSAGE_FAIL) {
        // write one byte to EEPROM
        I2C1_MasterWrite(writeBuffer,
                length + ADD_SIZE,
                romadd,
                &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING) {
            // add some delay here
            __delay_us(200);
            // timeout checking
            // check for max retry and skip this byte
            if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
                break;
            else
                slaveTimeOut++;
        }
        if ((slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT) ||
                (status == I2C1_MESSAGE_COMPLETE))
            break;

        // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
        //               or I2C1_DATA_NO_ACK,
        // The device may be busy and needs more time for the last
        // write so we can retry writing the data, this is why we
        // use a while loop here

        // check for max retry and skip this byte
        if (timeOut == SLAVE_I2C_GENERIC_RETRY_MAX)
            break;
        else
            timeOut++;
    }

    if (status != I2C1_MESSAGE_COMPLETE) {
        return (false);
    }
    return true;
}

bool romRead(uint8_t romadd, uint16_t add, uint8_t* data, uint8_t length) {
    static I2C1_MESSAGE_STATUS status;
    static uint8_t writeBuffer[ADD_SIZE];
    static uint16_t retryTimeOut, slaveTimeOut;



    // build the write buffer first
    // starting address of the EEPROM memory
#if(ADD_SIZE==1)
    writeBuffer[0] = (uint8_t) (add); // low low address
#else
    writeBuffer[0] = (add >> 8); // high address
    writeBuffer[1] = (uint8_t) (add); // low low address
#endif

    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    retryTimeOut = 0;
    slaveTimeOut = 0;
    status = I2C1_MESSAGE_PENDING;
    while (status != I2C1_MESSAGE_FAIL) {
        // write one byte to EEPROM (2 is the count of bytes to write)
        I2C1_MasterWrite(writeBuffer,
                ADD_SIZE,
                romadd,
                &status);

        // wait for the message to be sent or status has changed.
        while (status == I2C1_MESSAGE_PENDING) {
            // add some delay here
            __delay_us(200);
            // timeout checking
            // check for max retry and skip this byte
            if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
                return (false);
            else
                slaveTimeOut++;
        }

        if (status == I2C1_MESSAGE_COMPLETE)
            break;

        // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
        //               or I2C1_DATA_NO_ACK,
        // The device may be busy and needs more time for the last
        // write so we can retry writing the data, this is why we
        // use a while loop here

        // check for max retry and skip this byte
        if (retryTimeOut == SLAVE_I2C_GENERIC_RETRY_MAX)
            break;
        else
            retryTimeOut++;
    }

    if (status == I2C1_MESSAGE_COMPLETE) {

        // this portion will read the byte from the memory location.
        retryTimeOut = 0;
        slaveTimeOut = 0;

        while (status != I2C1_MESSAGE_FAIL) {
            // write one byte to EEPROM (2 is the count of bytes to write)
            I2C1_MasterRead(data,
                    length,
                    romadd,
                    &status);

            // wait for the message to be sent or status has changed.
            while (status == I2C1_MESSAGE_PENDING) {
                // add some delay here
                __delay_us(200);
                // timeout checking
                // check for max retry and skip this byte
                if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
                    return (false);
                else
                    slaveTimeOut++;
            }

            if (status == I2C1_MESSAGE_COMPLETE)
                break;

            // if status is  I2C1_MESSAGE_ADDRESS_NO_ACK,
            //               or I2C1_DATA_NO_ACK,
            // The device may be busy and needs more time for the last
            // write so we can retry writing the data, this is why we
            // use a while loop here

            // check for max retry and skip this byte
            if (retryTimeOut == SLAVE_I2C_GENERIC_RETRY_MAX)
                break;
            else
                retryTimeOut++;
        }
    }

    // exit if the last transaction failed
    if (status != I2C1_MESSAGE_COMPLETE) {
        return (false);
    }


    return (true);

}

bool isReady(uint8_t romadd) {
    static I2C1_MESSAGE_STATUS status;
    static uint16_t slaveTimeOut;
    slaveTimeOut = 0;
    status = I2C1_MESSAGE_PENDING;
    // write one byte to EEPROM (2 is the count of bytes to write)
    I2C1_MasterWrite(NULL,
            0,
            romadd,
            &status);

    // wait for the message to be sent or status has changed.
    while (status == I2C1_MESSAGE_PENDING) {
        // add some delay here
        __delay_us(200);
        // timeout checking
        // check for max retry and skip this byte
        if (slaveTimeOut == SLAVE_I2C_GENERIC_DEVICE_TIMEOUT)
            return (false);
        else
            slaveTimeOut++;
    }


    // exit if the last transaction failed
    if (status != I2C1_MESSAGE_COMPLETE) {
        return (false);
    }


    return (true);
}