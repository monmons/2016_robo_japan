/* 
 * File:   controller.h
 * Author: Shuuya
 *
 * Created on 2016/11/05, 10:33
 */

#ifndef CONTROLLER_H
#define	CONTROLLER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "function_2016_japancap.h"

    typedef union {
        uint8_t byte;

        struct {
            unsigned now_UART : 1; //送信中か否か
            unsigned now_data_presence : 1; //バッファにデータがあるか否か 
            unsigned now_data_space : 1; //バッファに空きがあるか否か
        };

    } state_Ctrl;

    typedef void (*UART_1byte_TX)(uint8_t); //実際にUARTで送り出すところ(1byte),送信中なら送らない
    typedef void (*UART_ST)(void);

    extern state_Ctrl now_state_Ctrl;
    extern uint8_t FIFO_ASCii[];
    extern UART_1byte_TX putchar_Controller; //送信する関数
    extern UART_ST stop_TX_Controller; //送信割り込みの停止


#define now_data_exist_Ctrl  (now_state_Ctrl.now_data_presence)  
#define set_data_exist_Ctrl()  {now_state_Ctrl.now_data_presence = true;}
#define now_data_none_Ctrl   (!now_state_Ctrl.now_data_presence)
#define set_data_none_Ctrl() {now_state_Ctrl.now_data_presence = false;}

#define now_space_exist_Ctrl  (now_state_Ctrl.now_data_space)  
#define set_space_exist_Ctrl()  {now_state_Ctrl.now_data_space = true;}
#define now_space_none_Ctrl   (!now_state_Ctrl.now_data_space)
#define set_space_none_Ctrl() {now_state_Ctrl.now_data_space = false;}    

    void init_state_Ctrl(void);
    void clearScreen_Ctrl();
    void set_next_mes_FIFO(uint8_t, uint8_t, const uint8_t*); //送りたい文字列を代入,バッファに格納
    void FIFO_send_Controller_ISR(void); //TXISRで呼び出し
    bool putmes(uint8_t);
#ifdef	__cplusplus
}
#endif

#endif	/* CONTROLLER_H */

