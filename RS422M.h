/* 
 * File:   RS422M.h
 * Author: Shuuya
 *
 * Created on 2016/03/08, 1:25
 */

#ifndef RS422M_H
#define	RS422M_H
//データベースの作成とrs422の通信のヘッダファイル

#include "defines_for_RS422.h"
#include "defines_for_PIC24F.h"
#include "PIC24FJxxxGC006_head.h"
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <libpic30.h>


#ifdef	__cplusplus
extern "C" {
#endif

    /*使用マイコン固有レジスタのマクロ*/

#ifndef RS422_TX_SW_TRIS
#define RS422_TX_SW_TRIS TRISBbits.TRISB15
#endif      

#ifndef RS422_TX_SW
#define RS422_TX_SW LATBbits.LATB15
#endif  

#define RS422_bps UART1_bps
#define RS422_spb ((1.0) / (RS422_bps))

#ifndef RS422_TXREG
#define RS422_TXREG U1TXREG
#endif

#ifndef RS422_RXREG
#define RS422_RXREG U1RXREG
#endif

#ifndef TXREG_forPC
#define TXREG_forPC U3TXREG
#endif

#ifndef RXREG_forPC
#define RS422_forPC U3RXREG
#endif

#ifndef RS422_TXIF
#define RS422_TXIF _U1TXIF
#endif

#ifndef RS422_TXIE
#define RS422_TXIE _U1TXIE
#endif

#ifndef RS422_TXIP
#define RS422_TXIP _U1TXIP
#endif    

#ifndef RS422_RXIF
#define RS422_RXIF _U1RXIF
#endif  
    
#ifndef RS422_RXIE
#define RS422_RXIE _U1RXIE
#endif

#ifndef RS422_RXIP
#define RS422_RXIP _U1RXIP
#endif        

#ifndef RS422_ADDEN
#define RS422_ADDEN U1STAbits.ADDEN
#endif  
    
#ifndef RS422_OERR
#define RS422_OERR U1STAbits.OERR
#endif    
    
#ifndef RS422_FERR
#define RS422_FERR U1STAbits.FERR
#endif     
    
#ifndef RS422_ENbit
#define RS422_ENbit U1MODEbits.UARTEN
#endif      

#ifndef RS422_TMR_ON_nOFF
#define RS422_TMR_ON_nOFF T5CONbits.TON
#endif    

#ifndef RS422_TMR
#define RS422_TMR TMR5
#endif
    
#define RS422_TMR_PR PR5

#ifndef MAX_TMR_COUNT
#define MAX_TMR_COUNT 65535.0
#endif

#ifndef RS422_TMR_TCKPS
#define RS422_TMR_TCKPS T5CONbits.TCKPS
#endif      

#ifndef RS422_TMRIE
#define RS422_TMRIE _T5IE
#endif    

#ifndef RS422_TMRIF
#define RS422_TMRIF _T5IF
#endif

#ifndef RS422_TMRIP
#define RS422_TMRIP _T5IP//優先度
#endif

#ifndef RS422_TMRIPnum
#define RS422_TMRIPnum TMR5_IP
#endif


    /*データベースの設定*/
#ifndef MAX_DATA_SIZE
#define MAX_DATA_SIZE 13
#endif

#ifndef MAX_DATA_KINDS
#define MAX_DATA_KINDS 11
#endif

#ifndef MAX_DEVICE_KINDS
#define MAX_DEVICE_KINDS 4
#endif

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (3+(MAX_DATA_SIZE))
#endif  
    //deviceID,dataID,length,datas

#ifndef delay_RS422
#define delay_RS422 ((((RS422_spb*11.0) * (4+MAX_DATA_SIZE + 10)) * milli * Fcyc) / (1.0))//3つの基本データ + MAX_DATA_SIZE + 1 +x
#endif 

    /*マクロ関数*/

    //#define set_data_type(deviceID_num,dataID_num,data_type) device_id[ deviceID_num ].data_id[ dataID_num ].status.type= data_type ;
    //データ群のデータタイプを指定する関数,引数：(deviceID,dataID,指定するdataタイプ)


#define set_ACK_RS422()     {start_reply();\
                            putUART_RS422(ACK_data, UART_data_type);\
                            now_buffer_position = 0;\
                            }

#define set_now_mes_again() {putUART_RS422(buffer_RS422M[0], UART_add_type); now_buffer_position = 1;}
#define search_deviceID     (index_valid[ now_rs422_position ].number_device_ID)    //現在参照している有効deviceID
#define search_dataID       (index_valid[ now_rs422_position ].number_data_ID)      //現在参照している有効dataID
#define search_length       (device_id[search_deviceID].data_id[search_dataID].length)      //現在参照している有効length
#define search_dataType      (device_id[search_deviceID].data_id[search_dataID].status.type)      //現在参照している有効dataタイプ
#define search_dataStatus      (state_RS422.now_RS422)      //現在通信の状態
#define search_OnceCommunication      (device_id[search_deviceID].data_id[search_dataID].status.Once_BidirectionalCommunication)      //現在参照している有効data状態
#define search_data_success (device_id[search_deviceID].data_id[search_dataID].status.CHECK)
    
#define start_transmit()      {search_dataStatus = now_transmit;\
    RS422_TXIF  = 0;RS422_TXIE  = 1;RS422_TX_SW = 1;}
    
#define start_reply()         {search_dataStatus = now_reply;\
    RS422_TXIF  = 0;RS422_TXIE  = 1;RS422_TX_SW = 1;}
    
#define start_recieve()       {search_dataStatus = now_recieve;\
    RS422_TXIF  = 0;RS422_TXIE  = 0;RS422_TX_SW = 0;recieve_times_rs422 = 0;}
    
#define start_idle()          {search_dataStatus = now_idle;\
    RS422_TXIF  = 0;RS422_TXIE  = 0;RS422_TX_SW = 0;}

#define putUART_RS422(datas,typeUART) {\
    RS422_TXIF = 0;\
    if (typeUART == UART_data_type) {\
        RS422_TXREG = (((uint16_t)datas) & 0x00FF);\
    } else {\
        RS422_TXREG = (((uint16_t)datas) | 0x0100);\
    }\
}


#define tmr_setup_RS422() {start_idle();\
                           if(( delay_RS422 ) < 0 ) {RS422_TMR_PR = delay_RS422;}\
                           else{RS422_TMR_PR=0xFFFF;}\
                           RS422_TMRIF = 0;RS422_TMRIP = RS422_TMRIPnum;\
                           RS422_TMRIE = 1;\
                           RS422_TMR_TCKPS = 0;\
                           RS422_TMR=0;\
                           RS422_TMR_ON_nOFF = 1;}   

#define tmr_ISR_RS422() {start_transmit();\
                        RS422_TMRIE = 0;\
                        RS422_TMRIF = 0;\
                        RS422_TMR_ON_nOFF = 0;\
                        now_buffer_position = 0;\
                        set_next_mes();\
                        }//タイマー割り込み関数にて呼び出すべき関数(送信間隔設定関数)

    typedef union {
        uint8_t byte;

        struct {
            unsigned now_RS422 : 4;
            unsigned final_reply_ToMe : 1;
            unsigned other : 4;
        };

    } data_state_RS422;

    typedef union {
        uint8_t byte;   

        struct {
            unsigned type : 3; //(dataの型)
            unsigned CHECK : 1; //通信の状態（成功か失敗か）
            unsigned Once_BidirectionalCommunication : 1; //<<1回のみ通信>>するかしないか
            unsigned OTHER : 3;
        };

    } data_status;

    typedef struct msgstr {
        uint8_t data[MAX_DATA_SIZE]; //(有効な命令もしくはデータの配列) ※宣言時、配列[n]={};ですべて0になります（知らんかった）
        uint8_t length; //dataの配列個数
        data_status status; //(受信データ=0　or 送信データ=1 or 無効なデータ=2)
    } rs422_data; //0~F:モータコントローラ

    typedef struct msgtxt {
        rs422_data data_id[MAX_DATA_KINDS];
    } rs422msg;

    typedef struct msgxy {
        uint8_t number_device_ID;
        uint8_t number_data_ID;
    } rs422_data_position;

    extern uint8_t loop_RS422; //loop状態
    extern data_state_RS422 state_RS422;
    extern uint8_t now_buffer_position;
    extern uint16_t now_rs422_position;
    extern uint16_t number_of_valid_data; //有効データ数 ※データが超膨大でないことを仮定(最大:short:32台,64個の32byteの配列)
    extern uint8_t buffer_RS422M[BUFFER_SIZE];
    extern rs422msg device_id[MAX_DEVICE_KINDS];
    extern rs422_data_position index_valid[(MAX_DEVICE_KINDS)*(MAX_DATA_KINDS)];

    void Start_RS422();
    void set_next_mes(); //次の送信データを準備する関数だが、プログラマは呼び出す必要はない
    void rx_ISR_RS422(); //受信割り込み関数にて呼び出すべき関数(定期通信データベース化関数)
    void tx_ISR_RS422(); //送信割り込み関数にて呼び出すべき関数（連続送信関数）
    void Initialization_datas_RS422(void);
    void reset_All_RS422(void);
#if 1
    /***************************************************************************************/
    /*EEPROM遠隔書き込み用 RS422とは違うUARTを用いる*/
    /***************************************************************************************/

#ifndef toEEPROM_TXREG
#define toEEPROM_TXREG U3TXREG
#endif

#ifndef toEEPROM_RXREG
#define toEEPROM_RXREG U3RXREG
#endif

#ifndef toEEPROM_TXIF
#define toEEPROM_TXIF _U3TXIF
#endif

#ifndef toEEPROM_TXIE
#define toEEPROM_TXIE _U3TXIE
#endif

#ifndef toEEPROM_RXIF
#define toEEPROM_RXIF _U3RXIF
#endif  

#ifndef toEEPROM_RXIE
#define toEEPROM_RXIE _U3RXIE
#endif  

    typedef struct msgtoEEPROM {
        uint8_t device_id;
        uint8_t data_id;
        uint8_t length;
        uint8_t data[16];
    } rs422_data_EEPROM; //EEPROM
    
#define I2CM_ISR_toEEPROM() {_MI2C1IF=0;\
        if(status_I2C == 0){\
            while(writeEEPROM_RS422M() == false);\
        }else if(status_I2C == 1){\
            while(readEEPROM_RS422M() == false);\
        }\
    }//I2Cマスタの割り込み関数で使用してください

#define putUART_toPC(datas){_U3TXIF = 0; U3TXREG = datas;}//
    
    extern rs422_data_EEPROM* datas_write;
    extern rs422_data* datas_read;
    extern uint8_t status_I2C;
    extern uint8_t adress_device, adress_data;
    extern uint16_t adress_EEPROM;
    extern rs422_data_EEPROM buffer_EEPROM[2]; //buffer_EEPROM[0]:書き込み用バッファ,buffer_EEPROM[1]:読み込み用バッファ

    bool writeEEPROM_RS422M(void);
    bool readEEPROM_RS422M(void);

    void rx_ISR_toEEPROM(void); //パソコンからEEPROMへの書き込みデータ受信関数(プロトコルはRS422と同じ)

    double* mecanum_Control_RS422(bool , int8_t , int8_t);
#endif
    
#ifdef	__cplusplus
}
#endif

#endif	/* RS422M_H */

